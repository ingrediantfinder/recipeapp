package api

import (
	"RecipeApp/utils"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/google/uuid"
)

func UploadFile(w http.ResponseWriter, r *http.Request) {
	fmt.Println("File Upload Endpoint Hit")

	cwd, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	uuid := uuid.New()
	imagepath := fmt.Sprintf("/images/%s.png", uuid.String())
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	imageFile, err := os.Create(fmt.Sprintf("%s%s", cwd, imagepath))
	if err != nil {
		errStr := fmt.Sprintf("Error os.Create: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}
	defer imageFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		errStr := fmt.Sprintf("Error os.Create: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}
	// write this byte array to our temporary file
	imageFile.Write(fileBytes)
	// return that we have successfully uploaded our file!
	fmt.Fprint(w, imagepath)
}
