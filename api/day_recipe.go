package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveDayRecipe implements endpoint POST /api/v1/day_recipe
func SaveDayRecipe(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var day_recipe db.DayRecipe
	err = json.Unmarshal(body, &day_recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal day_recipe: %v", err)
		http.Error(w, "can't unmarshal day_recipe", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveDayRecipe(ID, day_recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadIngredient implements endpoint GET /api/v1/DayRecipe/{DayRecipeID}
func ReadDayRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadDayRecipe"

	idParam := request.PathParam("day_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadDayRecipe(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllDayRecipes implements endpoint GET /api/v1/day_recipe
func ReadAllDayRecipes(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllDayRecipes"
	// Get all day_recipes
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllDayRecipes(1)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading DayRecipes: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateDayRecipe implements endpoint PUT /api/v1/day_recipe/{day_recipe}
func UpdateDayRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateDayRecipe"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var day_recipe db.DayRecipe
	err = json.Unmarshal(body, &day_recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal day_recipe: %v", err)
		http.Error(w, "can't unmarshal day_recipe", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("day_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	//var ID = tk.AccountID
	err = db.Manager.DS.UpdateDayRecipe(day_recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteDayRecipe  implements endpoint DEL /api/v1/day_recipe/{day_recipe}
func DeleteDayRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteDayRecipe"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	idParam := request.PathParam("day_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err = db.Manager.DS.DeleteDayRecipe(ID, id)
	if err != nil {
		utils.LogError("Failed to delete day_recipe")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
