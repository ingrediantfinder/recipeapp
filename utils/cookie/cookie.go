package cookie

import (
	"RecipeApp/constant"
	"net/http"

	"github.com/gorilla/securecookie"
)

// secure cookie
var secureCookie *securecookie.SecureCookie

// InitCookie initialize secure cookie
func InitCookie() {
	// Secure Cookie
	hashKey := []byte(constant.SecureCookieHash)
	secureCookie = securecookie.New(hashKey, nil)
}

// SetSecureCookie sets a secure cookie
func SetSecureCookie(w http.ResponseWriter, r *http.Request, cookieName, cookieValue string) error {
	encoded, err := secureCookie.Encode(cookieName, cookieValue)
	if err != nil {
		return err
	}
	cookie := &http.Cookie{
		Name:     cookieName,
		Value:    encoded,
		Path:     "/",
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
	//fmt.Printf("cookieName=%s, cookieValue=%s\n", cookieName, cookieValue)
	return nil
}

// ReadSecureCookie reads a secure cookie
func ReadSecureCookie(w http.ResponseWriter, r *http.Request, cookieName string) (string, bool) {
	cookie, err := r.Cookie(cookieName)
	if err != nil {
		return "", false
	}
	if cookie == nil {
		return "", false
	}
	// decode
	var value string
	err = secureCookie.Decode(cookieName, cookie.Value, &value)
	if err != nil {
		return "", false
	}
	return value, true
}
