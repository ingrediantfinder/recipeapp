package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

// -------------------------------
// response helper functions

// Error returns a json error response
func Error(w http.ResponseWriter, code int, message string) {
	JSON(w, code, map[string]string{"error": message})
}

// JSON set application/json header and returns a json response
func JSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		LogErrorf("[utils.JSON()]: err=%+v", err)
		errStr := fmt.Sprintf(`{"error": "%+v"}`, err)
		JSONStr(w, code, errStr)
		return
	}
	cResponse := new(bytes.Buffer)
	err = json.Compact(cResponse, response)
	if err != nil {
		LogErrorf("[utils.JSON()]: err=%+v", err)
		errStr := fmt.Sprintf(`{"error": "%+v"}`, err)
		JSONStr(w, code, errStr)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(code)
		w.Write(cResponse.Bytes())
	}
}

// JSONStr set application/json header and returns the input payload, which
// is already a JSON string
func JSONStr(w http.ResponseWriter, code int, payload string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(payload))
}
