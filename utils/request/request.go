// Package request has http helper functions
package request

import (
	"RecipeApp/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"

	"github.com/gorilla/mux"
)

// GetBodyAsBytes Gets this http request body (in its entirety) as a []byte
func GetBodyAsBytes(req *http.Request) ([]byte, error) {
	//Read req's body and convert byte[] to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		return nil, err
	}
	return body, nil
}

// UnmarshalJSONBody performs json.Unmarshal on the body of the given request.
// The given interface is used for the unmarshal scaffold and should be a
// pointer to some type.  The body is assumed to be required and matching the given
// structure.  If there is an I/O or parsing problem, a default http.StatusBadRequest
// response will be sent.
// Returns 'true' if the body was successfully retrieved, otherwise this response
// has been completed and the calling function should return
func UnmarshalJSONBody(w http.ResponseWriter, req *http.Request, v interface{}) bool {
	bodyBytes, err := GetBodyAsBytes(req)
	if err != nil {
		utils.Error(w, http.StatusInternalServerError, fmt.Sprintf("I/O error reading request body '%s'", err.Error()))
		utils.LogErrorf("error reading request body: %v", err)
		return false
	}
	err = json.Unmarshal(bodyBytes, v)
	if err != nil {
		utils.Error(w, http.StatusInternalServerError, fmt.Sprintf("Error parsing json body: '%s'", err.Error()))
		utils.LogErrorf("error parsing json request body: %v", err)
		return false
	}
	return true
}

// ParseParams is a convenience function to efficiently take a list of
// Param objects and find them in the request path.  If required params
// do not exist, an http bad request response will be sent on the given
// response writer with some useful canned information.
// Returns 'true' if all required params are avaialable and endpoint code
// execution should continue, otherwise an error response has already been
// sent and the calling function should return.
func ParseParams(w http.ResponseWriter, req *http.Request, params ...*Param) bool {
	if len(params) > 0 {
		var pathSet = mux.Vars(req)
		var queryMap map[string][]string = req.URL.Query()
		var headerMap map[string][]string = req.Header
		var err error
		for _, p := range params {
			switch p.Context {
			case "query":
				err = p.parseFromMultiMap(queryMap)
			case "path":
				err = p.parseFromSet(pathSet)
			case "header":
				err = p.parseFromMultiMap(headerMap)
			default:
				utils.Error(w, http.StatusInternalServerError, fmt.Sprintf("Unknown request param context '%s'", p.Context))
				return false
			}
			if err != nil {
				utils.Error(w, http.StatusBadRequest, fmt.Sprintf("Error parsing %s parameter '%s'.  Expected type '%s'", p.Context, p.Name, p.Format.String()))
				return false
			}
			if p.Required && p.IsEmpty() {
				utils.Error(w, http.StatusBadRequest, fmt.Sprintf("Required %s parameter '%s' missing.", p.Context, p.Name))
				return false
			}
		}
	}
	return true
}

// ParsePrimitiveStringValue attempts to convert an input string value
// to one of several supported primitive format.  Format is given as a reflect.Kind
// constant.  Currently supports string,int8,int16,int32,int64,float32,float64,bool,
// uint8,uint16,uint32,uint64
// Though the parsing of the primitive will be appropriate for the given format,
// the return value will be a generic interface{} type.
func ParsePrimitiveStringValue(str string, format reflect.Kind) (interface{}, error) {
	switch format {
	case reflect.String:
		return str, nil
	case reflect.Uint8:
		return strconv.ParseUint(str, 10, 8)
	case reflect.Uint16:
		return strconv.ParseUint(str, 10, 16)
	case reflect.Uint32:
		return strconv.ParseUint(str, 10, 32)
	case reflect.Uint64:
		return strconv.ParseUint(str, 10, 64)
	case reflect.Int8:
		return strconv.ParseInt(str, 10, 8)
	case reflect.Int16:
		return strconv.ParseInt(str, 10, 16)
	case reflect.Int32:
		return strconv.ParseInt(str, 10, 32)
	case reflect.Int64:
		return strconv.ParseInt(str, 10, 64)
	case reflect.Float32:
		return strconv.ParseFloat(str, 32)
	case reflect.Float64:
		return strconv.ParseFloat(str, 64)
	case reflect.Bool:
		return strconv.ParseBool(str)
	default:
		return nil, fmt.Errorf("Unsupported format '%s'", format.String())
	}
}
