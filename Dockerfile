# npm, node and ng are not installed
# $ cd client && npm install -g @angular/cli && cd .. is not run
# the required client/dist/*.js are build either by the developer or Jenkins

# Use an official Go image as the parent image
FROM golang:1.13.1

# Set the Go workspace
WORKDIR recipeapp

# Copy the current directory contents into the container at /app
ADD . recipeapp

# Install
RUN go install -v ./...

# Remove go src code so it is not visible in the docker container
RUN find . -name "*.go" -exec rm {} \;

# Remove data
#RUN rm -f data/*

# start bujo
#CMD ["bash", "-c", "bujo"]
CMD ["bujo"]

# Bulid Docker image
# docker build -f Dockerfile -t localhost:5000/bujo-local .

# docker image
# docker images

# docker run
# docker run -d --name localhost:5000/bujo -p 3000:3000 --rm localhost:5000/bujo-local

# docker view log
# docker logs --follow container_id

# attach to docker and view the container file system
# docker run --name bujo -p 3000:3000 --rm -it localhost:5000/bujo-local /bin/bash