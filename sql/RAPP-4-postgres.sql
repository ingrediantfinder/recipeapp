-- liquibase formatted sql

-- changeset tkuo:Recipe-4

ALTER TABLE shopping_list_ingredient DROP COLUMN recipe_id

-- rollback ALTER TABLE shopping_list_recipe add column recipe_id INTEGER NOT NULL;