package db

import (
	"RecipeApp/utils"
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	// initialize pq driver but not using it directly
	_ "github.com/lib/pq"
	"github.com/tidwall/gjson"
)

type postgresDS struct {
	db *sql.DB
}

var pgDS *postgresDS

// initializes postgres db for Bujo
func initPostgres() (DatastoreI, error) {
	dbHost := utils.GetSecret("db.host")
	dbPort := utils.GetSecret("db.port")
	dbUser := utils.GetSecret("db.user")
	dbMaxIdle := utils.GetSecret("db.maxidle")
	dbPassword := utils.GetSecret("db.password")
	dbName := utils.GetConfig("db.name")

	fmt.Printf("db.host=%s db.port=%d db.name=%s\n", dbHost, dbPort.Int(), dbName)
	fmt.Printf("db.password=%s\n", dbPassword)
	utils.LogInfo(fmt.Sprintf("db.host=%s db.port=%d db.name=%s", dbHost, dbPort.Int(), dbName))
	dbinfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		dbHost.String(), dbPort.Int(), dbUser, dbPassword, dbName)
	/*
		//[DirectToPostgres]
		dbinfo = "host=localhost port=5432 user=devuser password=devpassword dbname=devdb sslmode=disable"
	*/
	database, err := sql.Open("postgres", dbinfo)
	// if db.maxidle is provided, adjust the max number of idle tcp socket
	// connections available.  otherwise leave as default.  (local mac docker
	// workaround for broken idle connections, db.maxidle=0)
	if dbMaxIdle.Type == gjson.Number {
		database.SetMaxIdleConns(int(dbMaxIdle.Int()))
	}
	pgDS = &postgresDS{db: database}
	// ping
	err = pgDS.db.Ping()
	return pgDS, err
}

// ---------------------------------
// Account

func (pgDS *postgresDS) SaveAccount(account Account) (int32, error) {
	funcName := "postgres.SaveAccount"
	now := time.Now()
	insertStr := fmt.Sprintf(`INSERT INTO account
(firstname, lastname, email, lowercase_email, updated_at, hash)
VALUES ('%s', '%s', '%s', '%s', '%s', '%s')`,
		account.FirstName, account.LastName, account.Email, account.LowercaseEmail,
		utils.FormatTimestamp(now), account.Hash)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// Account's select-from clause
var accountSelectFromClause string = `SELECT id,firstname,lastname,email,lowercase_email,
updated_at, hash
FROM account`

// scanAccount scan the input rows and create and return a new Account for the row
func scanAccount(rows *sql.Rows) (Account, error) {
	funcName := "postgres.scanAccount"
	result := Account{}

	var id int32
	var firstName, lastName, email, lowercaseEmail string
	var updatedAt time.Time
	var hash string
	err := rows.Scan(
		&id, &firstName, &lastName, &email, &lowercaseEmail,
		&updatedAt, &hash)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Account{
		ID: id, FirstName: firstName, LastName: lastName, Email: email, LowercaseEmail: lowercaseEmail,
		UpdatedAt: updatedAt, Hash: hash,
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAccount(ids ...int32) ([]Account, error) {
	funcName := "postgres.ReadAccount"
	// prepare stmt
	var result []Account
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, accountSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Account
	for rows.Next() {
		account, err := scanAccount(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, account)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllAccounts() ([]Account, error) {
	funcName := "postgres.ReadAllAccounts"
	// prepare stmt
	var result []Account
	stmtStr := fmt.Sprintf(`%s
ORDER BY id`, accountSelectFromClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Account
	for rows.Next() {
		account, err := scanAccount(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, account)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAccountByEmail(email string) (Account, error) {
	funcName := "postgres.ReadAccountByEmail"
	lowercaseEmail := strings.ToLower(email)
	// prepare stmt
	account := Account{}
	stmtStr := fmt.Sprintf(`%s
WHERE lowercase_email='%s'`, accountSelectFromClause, lowercaseEmail)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return account, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return account, err
	}
	defer rows.Close()

	// loop through row and create Account
	if rows.Next() {
		account, err = scanAccount(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return account, err
		}
	} else {
		// No account found, error
		err = fmt.Errorf("No account found for email=%s", email)
		utils.LogError(fmt.Sprintf("[%s()] err=%s", funcName, err.Error()))
		return account, err
	}

	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return account, err
	}
	return account, nil
}

func (pgDS *postgresDS) UpdateAccount(account Account) error {
	funcName := "postgres.UpdateAccount"
	now := time.Now()
	// Update account
	updateStr := fmt.Sprintf(`UPDATE account SET
firstname='%s', lastname='%s', email='%s', lowercase_email='%s', 
updated_at='%s', hash='%s'
WHERE id = %d;`,
		account.FirstName, account.LastName, account.Email, account.LowercaseEmail,
		utils.FormatTimestamp(now), account.Hash,
		account.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteAccount(id int32) error {
	funcName := "postgres.DeleteAccount"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM account WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Ingredient

func (pgDS *postgresDS) SaveIngredient(ID int32, Ingredient Ingredient) (int32, error) {
	funcName := "postgres.SaveIngredient"
	now := time.Now()
	Ingredient.Name = strings.ToLower(Ingredient.Name)
	Ingredient.Name = strings.Title(Ingredient.Name)
	insertStr := fmt.Sprintf(`INSERT INTO Ingredient
(name, recipe_id,adjective, quantity, unit, updated_at)
VALUES ('%s', '%d', '%s', '%d','%s','%s')`,
		Ingredient.Name, ID, Ingredient.Adjective, Ingredient.Quantity, Ingredient.Unit,
		utils.FormatTimestamp(now))
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// Ingredient's select-from clause
var IngredientSelectFromClause string = `SELECT id,name, recipe_id, adjective, quantity, unit,
updated_at
FROM Ingredient`

var recipebyingredient string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction,  r.cooktime, r.updated_at
FROM recipe r JOIN Ingredient i ON r.id = i.recipe_id`

// scanIngredient
func scanIngredient(rows *sql.Rows) (Ingredient, error) {
	funcName := "postgres.scanIngredient"
	result := Ingredient{}

	var id, quantity, recipe_id int32
	var name, adjective, unit string
	var updatedAt time.Time
	err := rows.Scan(
		&id, &name, &recipe_id, &adjective, &quantity, &unit,
		&updatedAt)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Ingredient{
		ID: id, Name: name, RecipeID: recipe_id, Adjective: adjective, Quantity: quantity, Unit: unit, UpdatedAt: updatedAt,
	}
	return result, nil
}

// scanRecipe
func scanIRecipe(rows *sql.Rows) (Recipe, error) {
	funcName := "postgres.scanRecipe"
	result := Recipe{}

	var id, account_id int32
	var name, description, instruction, cooktime string
	var updatedAt time.Time
	err := rows.Scan(
		&id, &account_id, &name, &description, &instruction, &cooktime, &updatedAt)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Recipe{
		ID: id, AccountID: account_id, Name: name, Description: description, Instruction: instruction, Cooktime: cooktime, UpdatedAt: updatedAt,
	}
	return result, nil
}
func (pgDS *postgresDS) ReadIngredient(ids ...int32) ([]Ingredient, error) {
	funcName := "postgres.ReadIngredient"
	// prepare stmt
	var result []Ingredient
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, IngredientSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Ingredient
	for rows.Next() {
		Ingredient, err := scanIngredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) LeaveIngredientOut(rid int32, ids ...int32) ([]Ingredient, error) {
	funcName := "postgres.LeaveIngredientOut"
	// prepare stmt
	var result []Ingredient
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE NOT id in (%s) AND recipe_id = %d
ORDER BY id`, IngredientSelectFromClause, inClause, rid)
	//√

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Ingredient
	for rows.Next() {
		Ingredient, err := scanIngredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllIngredients(usrid int32) ([]Ingredient, error) {
	funcName := "postgres.ReadAllIngredients"
	// prepare stmt
	var result []Ingredient
	stmtStr := fmt.Sprintf(`%s WHERE recipe_id = %d`, IngredientSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Ingredient
	for rows.Next() {
		Ingredient, err := scanIngredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadRecipeFromIngredient(ingname string) ([]Recipe, error) {
	funcName := "postgres.ReadRecipeFromIngredient"
	// prepare stmt
	var result []Recipe
	var str = ingname + "%"
	fmt.Println(str)
	stmtStr := fmt.Sprintf(`%s WHERE i.name like '%s'`, recipebyingredient, str)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Ingredient
	for rows.Next() {
		Recipe, err := scanIRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}
func (pgDS *postgresDS) UpdateIngredient(rid int32, Ingredient Ingredient) error {
	funcName := "postgres.UpdateIngredient"
	now := time.Now()
	// Update Ingredient
	updateStr := fmt.Sprintf(`UPDATE Ingredient SET
name='%s', adjective = '%s', quantity='%d', unit = '%s' , updated_at='%s'
WHERE id = '%d' AND recipe_id = '%d';`,
		Ingredient.Name, Ingredient.Adjective, Ingredient.Quantity, Ingredient.Unit,
		utils.FormatTimestamp(now), Ingredient.ID, rid)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteIngredient(usrid int32, name string, rid int32) error {
	funcName := "postgres.DeleteIngredient"
	var buffer bytes.Buffer
	fmt.Println(name)
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM Ingredient WHERE name='%s' AND recipe_id = '%d';\n", name, rid))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Recipe
func (pgDS *postgresDS) SaveRecipe(ID int32, recipe Recipe) (int32, error) {
	funcName := "postgres.SaveRecipe"
	now := time.Now()
	insertStr := fmt.Sprintf(`INSERT INTO recipe
(name, account_id, description, instruction, cooktime, label_id, updated_at)
VALUES ('%s', '%d', '%s', '%s', '%s' , '%d', '%s')`,
		recipe.Name, ID, recipe.Description, recipe.Instruction, recipe.Cooktime, recipe.Label.ID,
		utils.FormatTimestamp(now))
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	RL := RecipeLabel{
		RecipeID: int32(id), LabelID: recipe.Label.ID,
	}
	pgDS.SaveRecipeLabel(0, RL)
	return int32(id), nil
}

// recipe's select-from clause
var recipeSelectFromClause string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction, r.cooktime, l.id, l.name, r.updated_at
FROM recipe r JOIN label l ON r.label_id = l.id`

// recipe's select-from clause
var recipeLabel string = `SELECT l.id, l.name FROM label l JOIN recipe_label rl ON l.id = rl.label_id JOIN recipe r ON rl.recipe_id = r.id`

var recipeSelectFromLabel string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction, r.cooktime,  l.id, l.name, r.updated_at
FROM recipe r JOIN recipe_label rl ON r.id = rl.recipe_id JOIN label l ON rl.label_id = l.id`

// recipe's select-from clause
var lastid string = `SELECT id FROM recipe WHERE ID = ( SELECT MAX(id) FROM recipe)`

// scanRecipe
func scanRecipe(rows *sql.Rows) (Recipe, error) {
	funcName := "postgres.scanRecipe"
	result := Recipe{}

	var id, account_id, lid int32
	var name, description, instruction, cooktime, lname string
	var updatedAt time.Time
	err := rows.Scan(
		&id, &account_id, &name, &description, &instruction, &cooktime, &lid, &lname, &updatedAt)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	Label := Label{
		ID: lid, Name: lname,
	}
	result = Recipe{
		ID: id, AccountID: account_id, Name: name, Description: description, Instruction: instruction, Cooktime: cooktime, UpdatedAt: updatedAt, Label: Label,
	}
	return result, nil
}

func (pgDS *postgresDS) ReadRecipe(ids ...int32) ([]Recipe, error) {
	funcName := "postgres.ReadRecipe"
	// prepare stmt
	var result []Recipe
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE r.id in (%s)
ORDER BY r.id`, recipeSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe
	for rows.Next() {
		recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

// scanRecipe
func scanid(rows *sql.Rows) (int, error) {
	funcName := "postgres.scanRecipe"
	result := 0

	var id int
	err := rows.Scan(
		&id)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = id
	return result, nil
}

func (pgDS *postgresDS) ReadAllRecipes(usrid int32) ([]Recipe, error) {
	funcName := "postgres.ReadAllRecipes"
	// prepare stmt
	var result []Recipe
	stmtStr := fmt.Sprintf(`%s ORDER BY r.name`, recipeSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe
	for rows.Next() {
		recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) SearchReadAllRecipes(usrid int32, str string) ([]Recipe, error) {
	funcName := "postgres.ReadAllRecipes"
	// prepare stmt
	var result []Recipe
	str = str + "%"
	stmtStr := fmt.Sprintf(`%s Where r.name like '%s' ORDER BY r.name`, recipeSelectFromClause, str)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe
	for rows.Next() {
		recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllRecipeLabel(usrid int32) ([]Label, error) {
	funcName := "postgres.ReadAllRecipeLabel"
	// prepare stmt
	var result []Label
	stmtStr := fmt.Sprintf(`%s WHERE rl.recipe_id = '%d'`, recipeLabel, usrid)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe
	for rows.Next() {
		recipe, err := scanLabel(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) SearchRecipe(usrid ...int32) ([]Recipe, error) {
	funcName := "postgres.SearchRecipe"
	// prepare stmt
	var result []Recipe
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(usrid), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE rl.label_id in (%s)`, recipeSelectFromLabel, inClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe
	for rows.Next() {
		recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateRecipe(ID int32, recipe Recipe) error {
	funcName := "postgres.UpdateRecipe"
	now := time.Now()
	// Update recipe
	updateStr := fmt.Sprintf(`UPDATE recipe SET
name='%s', description = '%s' , instruction = '%s', cooktime = '%s', label_id = '%d', updated_at='%s'
WHERE id = '%d' AND account_id = '%d';`,
		recipe.Name, recipe.Description, recipe.Instruction, recipe.Cooktime, recipe.Label.ID,
		utils.FormatTimestamp(now), recipe.ID, ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteRecipe(usrid int32, id int32) error {
	funcName := "postgres.DeleteRecipe"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM recipe WHERE id=%d;\n DELETE FROM ingredient WHERE recipe_id=%d;\n DELETE FROM recipe_label WHERE recipe_id = %d;\n", id, id, id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Label
func (pgDS *postgresDS) SaveLabel(ID int32, label Label) (int32, error) {
	funcName := "postgres.SaveLabel"

	insertStr := fmt.Sprintf(`INSERT INTO label
(name)
VALUES ('%s')`,
		label.Name)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// label's select-from clause
var labelSelectFromClause string = `SELECT id, name
FROM label`

// scanLabel
func scanLabel(rows *sql.Rows) (Label, error) {
	funcName := "postgres.scanLabel"
	result := Label{}

	var id int32
	var name string
	err := rows.Scan(
		&id, &name)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Label{
		ID: id, Name: name}
	return result, nil
}

func (pgDS *postgresDS) ReadLabel(ids ...int32) ([]Label, error) {
	funcName := "postgres.ReadLabel"
	// prepare stmt
	var result []Label
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, labelSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Label
	for rows.Next() {
		label, err := scanLabel(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, label)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllLabels(usrid int32) ([]Label, error) {
	funcName := "postgres.ReadAllLabels"
	// prepare stmt
	var result []Label
	stmtStr := fmt.Sprintf(`%s`, labelSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, labelSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Label
	for rows.Next() {
		label, err := scanLabel(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, label)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateLabel(label Label) error {
	funcName := "postgres.UpdateLabel"
	// Update label
	updateStr := fmt.Sprintf(`UPDATE label SET
name='%s'
WHERE id = '%d';`,
		label.Name, label.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteLabel(usrid int32, id int32) error {
	funcName := "postgres.DeleteLabel"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM label WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// RecipeLabel
func (pgDS *postgresDS) SaveRecipeLabel(ID int32, recipe_label RecipeLabel) (int32, error) {
	funcName := "postgres.SaveRecipeLabel"

	insertStr := fmt.Sprintf(`INSERT INTO recipe_label
(recipe_id, label_id)
VALUES ('%d', '%d')`,
		recipe_label.RecipeID, recipe_label.LabelID)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// recipe_label's select-from clause
var recipe_labelSelectFromClause string = `SELECT id, recipe_id, label_id
FROM recipe_label`

// scanRecipeLabel
func scanRecipeLabel(rows *sql.Rows) (RecipeLabel, error) {
	funcName := "postgres.scanRecipeLabel"
	result := RecipeLabel{}

	var id, recipe_id, label_id int32
	err := rows.Scan(
		&id, &recipe_id, &label_id)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = RecipeLabel{
		ID: id, RecipeID: recipe_id, LabelID: label_id}
	return result, nil
}

func (pgDS *postgresDS) ReadRecipeLabel(ids ...int32) ([]RecipeLabel, error) {
	funcName := "postgres.ReadRecipeLabel"
	// prepare stmt
	var result []RecipeLabel
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, recipe_labelSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create RecipeLabel
	for rows.Next() {
		recipe_label, err := scanRecipeLabel(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe_label)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllRecipeLabels(usrid int32) ([]RecipeLabel, error) {
	funcName := "postgres.ReadAllRecipeLabels"
	// prepare stmt
	var result []RecipeLabel
	stmtStr := fmt.Sprintf(`%s`, recipe_labelSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipe_labelSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create RecipeLabel
	for rows.Next() {
		recipe_label, err := scanRecipeLabel(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe_label)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateRecipeLabel(recipe_label RecipeLabel) error {
	funcName := "postgres.UpdateRecipeLabel"
	// Update recipe_label
	updateStr := fmt.Sprintf(`UPDATE recipe_label SET
recipe_id = '%d', label_id = '%d'
WHERE id = '%d';`,
		recipe_label.RecipeID, recipe_label.LabelID, recipe_label.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteRecipeLabel(usrid int32, id int32) error {
	funcName := "postgres.DeleteRecipeLabel"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM recipe_label WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Day
func (pgDS *postgresDS) SaveDay(ID int32, day Day) (int32, error) {
	funcName := "postgres.SaveDay"
	week_day := strings.Join([]string{strconv.Itoa(int(day.Week)), strconv.Itoa(int(day.Daynum))}, "")
	insertStr := fmt.Sprintf(`INSERT INTO day
(week, daynum, week_day)
VALUES ('%d', '%d', '%s')`,
		day.Week, day.Daynum, week_day)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// day's select-from clause
var daySelectFromClause string = `SELECT id, week, daynum, week_day
FROM day`

// recipe's select-from clause
var dayrecipe string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction, r.updated_at FROM recipe r JOIN day_recipe dr ON r.id = dr.recipe_id JOIN day d ON dr.day_id = d.id`

// scanDay
func scanDay(rows *sql.Rows) (Day, error) {
	funcName := "postgres.scanDay"
	result := Day{}

	var id, week, daynum int32
	var week_day string
	err := rows.Scan(
		&id, &week, &daynum, &week_day)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Day{
		ID: id, Week: week, Daynum: daynum, WeekDay: week_day}
	return result, nil
}

func (pgDS *postgresDS) ReadDay(ids ...int32) ([]Day, error) {
	funcName := "postgres.ReadDay"
	// prepare stmt
	var result []Day
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, daySelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Day
	for rows.Next() {
		day, err := scanDay(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, day)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllDays(usrid int32) ([]Day, error) {
	funcName := "postgres.ReadAllDays"
	// prepare stmt
	var result []Day
	stmtStr := fmt.Sprintf(`%s`, daySelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, daySelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Day
	for rows.Next() {
		day, err := scanDay(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, day)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadDayRecipes(usrid int32) ([]Recipe, error) {
	funcName := "postgres.ReadDayrecipes"
	// prepare stmt
	var result []Recipe
	stmtStr := fmt.Sprintf(`%s WHERE d.id = '%d'`, dayrecipe, usrid)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, daySelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Day
	for rows.Next() {
		recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateDay(day Day) error {
	funcName := "postgres.UpdateDay"
	// Update day
	week_day := fmt.Sprintf("%d%d", day.Week, day.Daynum)
	updateStr := fmt.Sprintf(`UPDATE day SET
week = '%d', daynum = '%d', week_day = '%s'
WHERE id = '%d';`,
		day.Week, day.Daynum, week_day, day.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteDay(usrid int32, id int32) error {
	funcName := "postgres.DeleteDay"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM day WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// DayRecipe
func (pgDS *postgresDS) SaveDayRecipe(ID int32, day_recipe DayRecipe) (int32, error) {
	funcName := "postgres.SaveDayRecipe"
	insertStr := fmt.Sprintf(`INSERT INTO day_recipe
(day_id, recipe_id)
VALUES ('%d', '%d')`,
		day_recipe.DayID, day_recipe.RecipeID)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// day_recipe's select-from clause
var day_recipeSelectFromClause string = `SELECT id, day_id, recipe_id
FROM day_recipe`
var recipefromlist string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction, r.cooktime, l.id, l.name, r.updated_at
FROM recipe r JOIN label l ON r.label_id = l.id`

// scanDayRecipe
func scanDayRecipe(rows *sql.Rows) (DayRecipe, error) {
	funcName := "postgres.scanDayRecipe"
	result := DayRecipe{}

	var id, day_id, recipe_id int32
	err := rows.Scan(
		&id, &day_id, &recipe_id)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = DayRecipe{
		ID: id, DayID: day_id, RecipeID: recipe_id}
	return result, nil
}

func (pgDS *postgresDS) ReadDayRecipe(ids ...int32) ([]DayRecipe, error) {
	funcName := "postgres.ReadDayRecipe"
	// prepare stmt
	var result []DayRecipe
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, day_recipeSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create DayRecipe
	for rows.Next() {
		day_recipe, err := scanDayRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, day_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllDayRecipes(usrid int32) ([]DayRecipe, error) {
	funcName := "postgres.ReadAllDayRecipes"
	// prepare stmt
	var result []DayRecipe
	stmtStr := fmt.Sprintf(`%s WHERE day_id = %d`, day_recipeSelectFromClause, usrid)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, day_recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create DayRecipe
	for rows.Next() {
		day_recipe, err := scanDayRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, day_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateDayRecipe(day_recipe DayRecipe) error {
	funcName := "postgres.UpdateDayRecipe"
	// Update day_recipe
	updateStr := fmt.Sprintf(`UPDATE day_recipe SET
day_id = '%d', recipe_id = '%d'
WHERE id = '%d';`,
		day_recipe.DayID, day_recipe.RecipeID, day_recipe.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteDayRecipe(usrid int32, id int32) error {
	funcName := "postgres.DeleteDayRecipe"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM day_recipe WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Recipe_image
func (pgDS *postgresDS) SaveRecipe_image(ID int32, recipe_image Recipe_image) (int32, error) {
	funcName := "postgres.SaveRecipe_image"

	now := time.Now()
	insertStr := fmt.Sprintf(`INSERT INTO recipe_image
(recipe_id, image_url, updated_at)
VALUES ('%d', '%s', '%s')`,
		recipe_image.Recipe_id, recipe_image.Image_url, utils.FormatTimestamp(now))
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// recipe_image's select-from clause
var recipe_imageSelectFromClause string = `SELECT id, recipe_id, image_url
FROM recipe_image`

// scanRecipe_image
func scanRecipe_image(rows *sql.Rows) (Recipe_image, error) {
	funcName := "postgres.scanRecipe_image"
	result := Recipe_image{}

	var id, r_id int32
	var i_url string
	err := rows.Scan(
		&id, &r_id, &i_url)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Recipe_image{
		ID: id, Recipe_id: r_id, Image_url: i_url}
	return result, nil
}

func (pgDS *postgresDS) ReadRecipe_image(ids ...int32) ([]Recipe_image, error) {
	funcName := "postgres.ReadRecipe_image"
	// prepare stmt
	var result []Recipe_image
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, recipe_imageSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe_image
	for rows.Next() {
		recipe_image, err := scanRecipe_image(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe_image)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllRecipe_images(usrid int32) ([]Recipe_image, error) {
	funcName := "postgres.ReadAllRecipe_images"
	// prepare stmt
	var result []Recipe_image
	stmtStr := fmt.Sprintf(`%s`, recipe_imageSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, recipe_imageSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Recipe_image
	for rows.Next() {
		recipe_image, err := scanRecipe_image(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, recipe_image)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateRecipe_image(recipe_image Recipe_image) error {
	funcName := "postgres.UpdateRecipe_image"
	// Update recipe_image
	updateStr := fmt.Sprintf(`UPDATE recipe_image SET
image_url='%s'
WHERE recipe_id = '%d';`,
		recipe_image.Image_url, recipe_image.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteRecipe_image(usrid int32, id int32) error {
	funcName := "postgres.DeleteRecipe_image"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM recipe_image WHERE recipe_id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// shopping_list
func (pgDS *postgresDS) SaveShopping_list(ID int32, shopping_list Shopping_list) (int32, error) {
	funcName := "postgres.SaveShopping_list"
	insertStr := fmt.Sprintf(`INSERT INTO shopping_list
(user_id, name)
VALUES ('%d', '%s')`,
		shopping_list.User_id, shopping_list.Name)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// shopping_list's select-from clause
var shopping_listSelectFromClause string = `SELECT id, user_id, name
FROM shopping_list`

// scanShopping_list
func scanShopping_list(rows *sql.Rows) (Shopping_list, error) {
	funcName := "postgres.scanShopping_list"
	result := Shopping_list{}

	var id, user_id int32
	var name string
	err := rows.Scan(
		&id, &user_id, &name)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Shopping_list{
		ID: id, User_id: user_id, Name: name}
	return result, nil
}

func (pgDS *postgresDS) ReadShopping_list(ids ...int32) ([]Shopping_list, error) {
	funcName := "postgres.ReadShopping_list"
	// prepare stmt
	var result []Shopping_list
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, shopping_listSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list
	for rows.Next() {
		shopping_list, err := scanShopping_list(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadAllShopping_lists(usrid int32) ([]Shopping_list, error) {
	funcName := "postgres.ReadAllShopping_lists"
	// prepare stmt
	var result []Shopping_list
	stmtStr := fmt.Sprintf(`%s`, shopping_listSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, labelSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Label
	for rows.Next() {
		shopping_list, err := scanShopping_list(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateShopping_list(shopping_list Shopping_list) error {
	funcName := "postgres.Shopping_list"
	// Update recipe_image
	var name = strings.Replace(shopping_list.Name, "'", "''", -1)
	updateStr := fmt.Sprintf(`UPDATE shopping_list SET
	name='%s'
WHERE id = '%d';`,
		name, shopping_list.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteShopping_list(usrid int32, id int32) error {
	funcName := "postgres.DeleteShopping_list"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM shopping_list WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// ---------------------------------
// Shopping_list_ingredient

func (pgDS *postgresDS) SaveShopping_list_ingredient(ID int32, Shopping_list_ingredient Shopping_list_ingredient) (int32, error) {
	funcName := "postgres.SaveShopping_list_ingredient"
	now := time.Now()
	Shopping_list_ingredient.Name = strings.ToLower(Shopping_list_ingredient.Name)
	Shopping_list_ingredient.Name = strings.Title(Shopping_list_ingredient.Name)
	insertStr := fmt.Sprintf(`INSERT INTO Shopping_list_ingredient
(shopping_list, name,adjective, quantity, unit, updated_at)
VALUES ('%d','%s', '%s', '%d','%s','%s')`,
		Shopping_list_ingredient.Shopping_list, Shopping_list_ingredient.Name, Shopping_list_ingredient.Adjective, Shopping_list_ingredient.Quantity, Shopping_list_ingredient.Unit,
		utils.FormatTimestamp(now))
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// Shopping_list_ingredient's select-from clause
var Shopping_list_ingredientSelectFromClause string = `SELECT id, shopping_list, name, adjective, quantity, unit,
updated_at
FROM Shopping_list_ingredient`

// scanShopping_list_ingredient
func scanShopping_list_ingredient(rows *sql.Rows) (Shopping_list_ingredient, error) {
	funcName := "postgres.scanShopping_list_ingredient"
	result := Shopping_list_ingredient{}

	var id, quantity, shopping_list int32
	var name, adjective, unit string
	var updatedAt time.Time
	err := rows.Scan(
		&id, &shopping_list, &name, &adjective, &quantity, &unit,
		&updatedAt)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Shopping_list_ingredient{
		ID: id, Shopping_list: shopping_list, Name: name, Adjective: adjective, Quantity: quantity, Unit: unit, UpdatedAt: updatedAt,
	}
	return result, nil
}

func (pgDS *postgresDS) ReadShopping_list_ingredient(ids ...int32) ([]Shopping_list_ingredient, error) {
	funcName := "postgres.ReadShopping_list_ingredient"
	// prepare stmt
	var result []Shopping_list_ingredient
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE id in (%s)
ORDER BY id`, Shopping_list_ingredientSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_ingredient
	for rows.Next() {
		Shopping_list_ingredient, err := scanShopping_list_ingredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Shopping_list_ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}
func (pgDS *postgresDS) ReadAllShopping_list_ingredients(usrid int32) ([]Shopping_list_ingredient, error) {
	funcName := "postgres.ReadAllShopping_list_ingredients"
	// prepare stmt
	var result []Shopping_list_ingredient
	stmtStr := fmt.Sprintf(`%s WHERE shopping_list = '%d' `, Shopping_list_ingredientSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_ingredient
	for rows.Next() {
		Shopping_list_ingredient, err := scanShopping_list_ingredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Shopping_list_ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateShopping_list_ingredient(Shopping_list_ingredient Shopping_list_ingredient) error {
	funcName := "postgres.UpdateShopping_list_ingredient"
	now := time.Now()
	// Update Shopping_list_ingredient
	updateStr := fmt.Sprintf(`UPDATE Shopping_list_ingredient SET
name='%s', adjective = '%s', quantity='%d', unit = '%s' , updated_at='%s'
WHERE id = '%d';`,
		Shopping_list_ingredient.Name, Shopping_list_ingredient.Adjective, Shopping_list_ingredient.Quantity, Shopping_list_ingredient.Unit,
		utils.FormatTimestamp(now), Shopping_list_ingredient.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteShopping_list_ingredient(usrid int32, rid int32) error {
	funcName := "postgres.DeleteShopping_list_ingredient"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM Shopping_list_ingredient Where id = '%d';\n", rid))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}

// scanShopping_list_ingredient
func scaningredientlist(rows *sql.Rows) (Shopping_list_ingredient, error) {
	funcName := "postgres.scanShopping_list_ingredient"
	result := Shopping_list_ingredient{}

	var quantity int32
	var name, unit string
	err := rows.Scan(
		&name, &quantity, &unit)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Shopping_list_ingredient{
		Name: name, Quantity: quantity, Unit: unit,
	}
	return result, nil
}

// Shopping_list_ingredient's select-from clause
var Shopping_list_ingredientSelecttotals string = `SELECT name, SUM(quantity) as total_ingredients, unit FROM Shopping_list_ingredient GROUP BY name, unit`

func (pgDS *postgresDS) ReadIngredientTotals(usrid int32) ([]Shopping_list_ingredient, error) {
	funcName := "postgres.ReadIngredientTotals"
	// prepare stmt
	var result []Shopping_list_ingredient
	stmtStr := fmt.Sprintf(`%s `, Shopping_list_ingredientSelecttotals)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_ingredient
	for rows.Next() {
		Shopping_list_ingredient, err := scaningredientlist(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, Shopping_list_ingredient)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

// ---------------------------------
// Shopping_list_recipe
func (pgDS *postgresDS) SaveShopping_list_recipe(ID int32, shopping_list_recipe Shopping_list_recipe) (int32, error) {
	funcName := "postgres.SaveShopping_list_recipe"
	insertStr := fmt.Sprintf(`INSERT INTO shopping_list_recipe
(shopping_list_id, recipe_id)
VALUES ('%d', '%d')`,
		shopping_list_recipe.Shopping_listID, shopping_list_recipe.RecipeID)
	//fmt.Println(insertStr)
	var buffer bytes.Buffer
	buffer.WriteString(insertStr)

	var id int64
	driver := reflect.ValueOf(pgDS.db.Driver()).Type().String()
	if driver == "*sqlite3.SQLiteDriver" {
		buffer.WriteString(";\n")
		res, err := pgDS.db.Exec(buffer.String())
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
		returnID, error := res.LastInsertId()
		if error != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, error))
			return 0, error
		}
		id = returnID

	} else {
		buffer.WriteString(" RETURNING id;\n")
		err := pgDS.db.QueryRow(buffer.String()).Scan(&id)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
			return 0, err
		}
	}
	return int32(id), nil
}

// shopping_list_recipe's select-from clause
var shopping_list_recipeSelectFromClause string = `SELECT id, shopping_list_id, recipe_id
FROM shopping_list_recipe`

// shopping_list_recipe's select-from clause
var get_recipe_from_shopping_list_recipeSelectFromClause string = `SELECT r.id,r.account_id, r.name,r.description, r.instruction, r.cooktime, l.id, l.name, r.updated_at
FROM recipe r JOIN label l ON r.label_id = l.id JOIN shopping_list_recipe sr ON r.id = sr.recipe_id`

// scanShopping_list_recipe
func scanShopping_list_recipe(rows *sql.Rows) (Shopping_list_recipe, error) {
	funcName := "postgres.scanShopping_list_recipe"
	result := Shopping_list_recipe{}

	var id, shopping_list_id, recipe_id int32
	err := rows.Scan(
		&id, &shopping_list_id, &recipe_id)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Shopping_list_recipe{
		ID: id, Shopping_listID: shopping_list_id, RecipeID: recipe_id}
	return result, nil
}

var getingbyrecipes string = `SELECT i.name, i.adjective, SUM(i.quantity), i.unit 
FROM Ingredient i JOIN recipe r ON i.recipe_id = r.id JOIN shopping_list_recipe sr ON r.id = sr.recipe_id`

//GROUP BY i.name, i.adjective, i.unit ORDER BY i.name

// scanShopping_list_recipe
func scanShopping_list_recipe_ingredient(rows *sql.Rows) (Ingredient, error) {
	funcName := "postgres.scanShopping_list_recipe"
	result := Ingredient{}

	var quantity int32
	var name, adjective, unit string
	err := rows.Scan(
		&name, &adjective, &quantity, &unit,
	)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
		return result, err
	}
	result = Ingredient{
		Name: name, Adjective: adjective, Quantity: quantity, Unit: unit,
	}
	return result, nil
}

func (pgDS *postgresDS) ReadShopping_list_recipe(ids ...int32) ([]Recipe, error) {
	funcName := "postgres.ReadShopping_list_recipe"
	// prepare stmt
	var result []Recipe
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE shopping_list_id in (%s)
ORDER BY r.id`, get_recipe_from_shopping_list_recipeSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_recipe
	for rows.Next() {
		shopping_list_recipe, err := scanRecipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadShopping_list_by_recipe(ids ...int32) ([]Shopping_list_recipe, error) {
	funcName := "postgres.ReadShopping_list_recipe"
	// prepare stmt
	var result []Shopping_list_recipe
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE recipe_id in (%s)
ORDER BY id`, shopping_list_recipeSelectFromClause, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_recipe
	for rows.Next() {
		shopping_list_recipe, err := scanShopping_list_recipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) ReadShopping_list_recipe_ingredient(ids ...int32) ([]Ingredient, error) {
	funcName := "postgres.ReadShopping_list_recipe"
	// prepare stmt
	var result []Ingredient
	inClause := strings.Trim(strings.Join(strings.Split(fmt.Sprint(ids), " "), ","), "[]")
	stmtStr := fmt.Sprintf(`%s
WHERE shopping_list_id in (%s)
GROUP BY i.name, i.adjective, i.unit ORDER BY i.name`, getingbyrecipes, inClause)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_recipe
	for rows.Next() {
		shopping_list_recipe, err := scanShopping_list_recipe_ingredient(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}
func (pgDS *postgresDS) ReadAllShopping_list_recipes(usrid int32) ([]Shopping_list_recipe, error) {
	funcName := "postgres.ReadAllShopping_list_recipes"
	// prepare stmt
	var result []Shopping_list_recipe
	stmtStr := fmt.Sprintf(`%s `, shopping_list_recipeSelectFromClause)
	//stmtStr := fmt.Sprintf(`%s WHERE r.id = %d`, shopping_list_recipeSelectFromClause, usrid)
	//fmt.Println(stmtStr)

	// ORDER BY is needed to make sure data we get back from db is in order of formations
	stmt, err := pgDS.db.Prepare(stmtStr)
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer stmt.Close()

	// query
	var rows *sql.Rows
	rows, err = stmt.Query()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	defer rows.Close()

	// loop through row and create Shopping_list_recipe
	for rows.Next() {
		shopping_list_recipe, err := scanShopping_list_recipe(rows)
		if err != nil {
			utils.LogError(fmt.Sprintf("[%s() err=%+v", funcName, err))
			return result, err
		}
		result = append(result, shopping_list_recipe)
	}
	// check error at the end for row.next() to avoid calling rows.Close() inducing a runtime panic
	err = rows.Err()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return result, err
	}
	return result, nil
}

func (pgDS *postgresDS) UpdateShopping_list_recipe(shopping_list_recipe Shopping_list_recipe) error {
	funcName := "postgres.UpdateShopping_list_recipe"
	// Update shopping_list_recipe
	updateStr := fmt.Sprintf(`UPDATE shopping_list_recipe SET
	shopping_list_id = '%d', recipe_id = '%d'
WHERE id = '%d';`,
		shopping_list_recipe.Shopping_listID, shopping_list_recipe.RecipeID, shopping_list_recipe.ID)
	//fmt.Println(updateStr)
	var buffer bytes.Buffer
	buffer.WriteString(updateStr)

	res, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	if rowsAffected == 0 {
		utils.LogError(fmt.Sprintf("[%s()] 0 rows effected with update operation", funcName))
		return errors.New("noRowFound")
	}
	return nil
}

func (pgDS *postgresDS) DeleteShopping_list_recipe(usrid int32, id int32) error {
	funcName := "postgres.DeleteShopping_list_recipe"
	var buffer bytes.Buffer
	buffer.WriteString(
		fmt.Sprintf("DELETE FROM shopping_list_recipe WHERE id=%d\n", id))
	_, err := pgDS.db.Exec(buffer.String())
	if err != nil {
		utils.LogError(fmt.Sprintf("[%s()] err=%+v", funcName, err))
		return err
	}
	return nil
}
