package utils

import (
	"fmt"
	"strings"
	"time"
)

/*
This util should be used for all logging - here is where we can control the logger interface
to stdout / stderr and make adjustments for things like tags or line endings.

For example, docker and kubernetes will ingest stdout/stderr line by line, so we have to have
a way to put multiline messages back together. Here, we insert 2 tabs after each additional
line, and logstash can use that to put the message back together
*/

// LogLevelDebug sets logging to the debug level
const LogLevelDebug string = "debug"

// LogLevelInfo sets logging to the info level
const LogLevelInfo string = "info"

// LogLevelWarn sets logging to the warn level
const LogLevelWarn string = "warn"

// LogLevelError sets logging to the error level
const LogLevelError string = "error"

// LogDebug logs information at an 'debug' level, to stdout
func LogDebug(message string) {
	logCore(message, LogLevelDebug)
}

// LogDebugf logs information at an 'debug' level, to stdout using formatting
func LogDebugf(message string, a ...interface{}) {
	logCore(message, LogLevelDebug, a...)
}

// LogInfo logs information at an 'info' level, to stdout
func LogInfo(message string) {
	logCore(message, LogLevelInfo)
}

// LogInfof logs information at an 'info' level, to stdout using formatting
func LogInfof(message string, a ...interface{}) {
	logCore(message, LogLevelInfo, a...)
}

// LogWarn logs information at an 'warn' level, to stdout
func LogWarn(message string) {
	logCore(message, LogLevelWarn)
}

// LogWarnf logs information at an 'warn' level, to stdout using formatting
func LogWarnf(message string, a ...interface{}) {
	logCore(message, LogLevelWarn, a...)
}

// LogError logs information at an 'error' level, to stderr
func LogError(message string) {
	logCore(message, LogLevelError)
}

// LogErrorf logs information at an 'error' level, to stderr using formatting
func LogErrorf(message string, a ...interface{}) {
	logCore(message, LogLevelError, a...)
}

var minLogLevelInt int32

/*
	Generic logging function that handles all levels
*/
func logCore(message string, level string, a ...interface{}) {

	if minLogLevelInt == 0 {
		minLogLevel := GetSecret("log.level").String()

		minLogLevelIntFromFile, minLogLevelErr := logLevelValue(minLogLevel)
		if minLogLevelErr != nil {
			fmt.Println("Cannot read configured Log Level: " + minLogLevelErr.Error())
			return
		}

		minLogLevelInt = minLogLevelIntFromFile
	}

	currentLogLevelInt, currentLogLevelErr := logLevelValue(level)
	if currentLogLevelErr != nil {
		fmt.Println("Unknown Log Level: " + currentLogLevelErr.Error())
	}

	if currentLogLevelInt < minLogLevelInt {
		return
	}

	var formattedLogMessage string
	if a != nil {
		formattedLogMessage = fmt.Sprintf(message, a...)
	} else {
		formattedLogMessage = message
	}

	logMessage := fmt.Sprintf("[%s] %s %s", level, time.Now().UTC().Format("2006-01-02 15:04:05"), strings.Replace(formattedLogMessage, "\n", "\n\t\t", -1))
	fmt.Println(logMessage)
}

func logLevelValue(logLevel string) (int32, error) {

	normalized := strings.ToLower(strings.TrimSpace(logLevel))
	if strings.ToLower(normalized) == LogLevelDebug {
		return 1, nil

	} else if strings.ToLower(normalized) == LogLevelInfo {
		return 2, nil

	} else if strings.ToLower(normalized) == LogLevelWarn {
		return 3, nil

	} else if strings.ToLower(normalized) == LogLevelError {
		return 4, nil

	}

	return 0, fmt.Errorf("Unknown Logging Level: '%s'", logLevel)
}
