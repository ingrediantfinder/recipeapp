package db

import (
	"RecipeApp/utils"
	"database/sql"
	"fmt"
)

// SetupManager sets Manager to pgDS
func SetupManager() {
	Manager = &ManagerT{DS: pgDS}
}

// SetupTestDataDS opens a sqlite3 db, drop and create json_data table, and set pgDS.
func SetupTestDataDS(dbFilename string, tables []string) {
	// Create a sqlite3 db
	connectionStr := fmt.Sprintf("%s?_foreign_keys=on", dbFilename)
	database, err := sql.Open("sqlite3", connectionStr)
	utils.CheckErr(err)

	// ping
	err = database.Ping()
	utils.CheckErr(err)

	for _, table := range tables {
		if table == "account" {
			SetupAccountTable(database)
		} else if table == "ingredient" {
			SetupIngredientTable(database)
		} else if table == "recipe" {
			SetupRecipeTable(database)
		} else if table == "label" {
			SetupLabelTable(database)
		} else if table == "recipe_label" {
			SetupRecipeLabelTable(database)
		} else if table == "day" {
			SetupDay(database)
		} else if table == "day_recipe" {
			SetupDayRecipe(database)
		}
	}
	// set pgDS to db to run statements against sqlite3
	pgDS = &postgresDS{db: database}
}

// SetupAccountTable opens a sqlite3 db, drop and create account table, and set pgDS.
func SetupAccountTable(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS account;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS account (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			firstname VARCHAR(80) NOT NULL,
			lastname VARCHAR(80),
			email VARCHAR(80),
			lowercase_email VARCHAR(80) NOT NULL,
			hash VARCHAR(255) NOT NULL,
			updated_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

// SetupIngredientTable opens a sqlite3 db, drop and create Ingredient table, and set pgDS.
func SetupIngredientTable(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS ingredient;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS ingredient (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			recipe_id INTEGER NOT NULL,
			name VARCHAR(255) NOT NULL,
			description TEXT,
			adjective VARCHAR(255),
			quantity INTEGER NOT NULL,
			unit VARCHAR(255),
			updated_at DATETIME,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

// SetupLabelTable opens a sqlite3 db, drop and create recipe table, and set pgDS.
func SetupRecipeTable(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS recipe;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS recipe (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			account_id INTEGER,
			name VARCHAR(255) NOT NULL,
			description TEXT,
			label_id INTEGER,
			instruction TEXT,
			updated_at DATETIME,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

func SetupLabelTable(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS label;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS label (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			name VARCHAR(255) NOT NULL,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

func SetupRecipeLabelTable(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS recipe_label;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS recipe_label (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			recipe_id INTEGER,
			label_id INTEGER,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

func SetupDay(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS day;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS day (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			week INTEGER,
			daynum INTEGER,
			week_day VARCHAR(255) UNIQUE,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

func SetupDayRecipe(database *sql.DB) {
	// Drop the database
	dropScript := `DROP TABLE IF EXISTS day_recipe;`
	stmt, err := database.Prepare(dropScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)

	// Create the database.
	createScript := `CREATE TABLE IF NOT EXISTS day_recipe (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			day_id INTEGER,
			recipe_id INTEGER,
			created_at DATETIME
		);`
	stmt, err = database.Prepare(createScript)
	utils.CheckErr(err)
	_, err = stmt.Exec()
	utils.CheckErr(err)
}

// CloseDataDS closes the pgDS.
func CloseDataDS() {
	pgDS.db.Close()
}

// CloseTestDataDS closes the pgDS.
func CloseTestDataDS() {
	pgDS.db.Close()
}
