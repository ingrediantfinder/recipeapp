package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveLabel implements endpoint POST /api/v1/label
func SaveLabel(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var label db.Label
	err = json.Unmarshal(body, &label)
	if err != nil {
		utils.LogErrorf("Error unmarshal label: %v", err)
		http.Error(w, "can't unmarshal label", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveLabel(ID, label)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadLabel implements endpoint GET /api/v1/label/{label}
func ReadLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadLabel"

	idParam := request.PathParam("labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Label
	results, err := db.Manager.DS.ReadLabel(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Label: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllLabels implements endpoint GET /api/v1/label
func ReadAllLabels(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllLabels"
	// Get all labels
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllLabels(ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Labels: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateLabel implements endpoint PUT /api/v1/label/{label}
func UpdateLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateLabel"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var label db.Label
	err = json.Unmarshal(body, &label)
	if err != nil {
		utils.LogErrorf("Error unmarshal label: %v", err)
		http.Error(w, "can't unmarshal label", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()
	label.ID = idParam.AsInt32()
	err = db.Manager.DS.UpdateLabel(label)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteLabel  implements endpoint DEL /api/v1/label/{label}
func DeleteLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteLabel"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	idParam := request.PathParam("labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	err = db.Manager.DS.DeleteLabel(ID, id)
	if err != nil {
		utils.LogError("Failed to delete label")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
