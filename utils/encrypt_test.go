package utils

import (
	"fmt"
	"testing"
)

func TestPasswd(t *testing.T) {
	Initialize("..")

	passwd1 := "foobar"
	passwd1a := "foobar" // should have different hash and salt
	passwd2 := "foobarbaz"
	// generate hashed password with salt
	hashedPasswd1, err := HashAndSalt(passwd1)
	if err != nil {
		t.Fatalf(err.Error())
	}
	fmt.Printf("hashed and salted passwd1 = %s\n", hashedPasswd1)
	hashedPasswd1a, err := HashAndSalt(passwd1a)
	if err != nil {
		t.Fatalf(err.Error())
	}
	fmt.Printf("hashed and salted passwd1a = %s\n", hashedPasswd1a)
	hashedPasswd2, err := HashAndSalt(passwd2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	fmt.Printf("hashed and salted passwd2 = %s\n", hashedPasswd2)

	// compare
	// should validate
	if !ComparePasswords(hashedPasswd1, passwd1) {
		t.Fatalf("password %s should be correct", passwd1)
	}
	if !ComparePasswords(hashedPasswd1a, passwd1) {
		t.Fatalf("password %s should be correct", passwd1a)
	}
	if !ComparePasswords(hashedPasswd1, passwd1a) { // differet hash and salt but still validate with "foobar"
		t.Fatalf("password %s should be incorrect", passwd1a)
	}
	if !ComparePasswords(hashedPasswd2, passwd2) {
		t.Fatalf("password %s should be correct", passwd2)
	}
	// should not validate
	if ComparePasswords(hashedPasswd1, passwd2) {
		t.Fatalf("password %s should be incorrect", passwd2)
	}
	if ComparePasswords(hashedPasswd2, passwd1) {
		t.Fatalf("password %s should be incorrect", passwd1)
	}
}
