package db

import (
	"RecipeApp/utils"
	"fmt"
	"testing"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

// --------------------------------
// Account
func TestSaveAccount(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"account"})
	defer CloseTestDataDS()

	// create test cases
	cases := []Account{
		{FirstName: "Conan", LastName: "Kuo", Email: "ckuo@bujo.com", LowercaseEmail: "ckuo@bujo.com", Hash: "hash_ckuo@bujo.com"},
		{FirstName: "Ran", LastName: "Kuo", Email: "rkuo@bujo.com", LowercaseEmail: "rkuo@bujo.com", Hash: "hash_rkuo@bujo.com"},
	}

	// (0) save
	for i := range cases {
		id, err := pgDS.SaveAccount(cases[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases[i].ID = id
	}

	// (1) Read all accounts
	dbAccounts, err := pgDS.ReadAllAccounts()
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateAccounts(t, cases[0:], dbAccounts)

	// (2) Update Ran
	cases[1].SetEmail("rankuo@bujo.com")
	cases[1].Hash = "hash_rankuo@bjuo.com"
	err = pgDS.UpdateAccount(cases[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	// (3) Read "Ran"
	dbAccounts, err = pgDS.ReadAccount(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateAccounts(t, cases[1:], dbAccounts)

	// (4) Read "Conan" by email
	dbAccount, err := pgDS.ReadAccountByEmail(cases[0].Email)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateAccounts(t, cases[:1], []Account{dbAccount})

	// (5) Delete Ran
	pgDS.DeleteAccount(cases[1].ID)
	dbAccounts, err = pgDS.ReadAccount(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbAccounts) != 0 {
		t.Fatalf("Account ID=%d was not deleted properly", cases[1].ID)
	}
}

func validateAccounts(t *testing.T, expAccounts, accounts []Account) {
	if len(expAccounts) != len(accounts) {
		t.Fatalf("len(expAccounts) %d != len(accounts) %d\n", len(expAccounts), len(accounts))
	}

	asserted := 0
	for _, v := range expAccounts {
		for _, v2 := range accounts {
			if v.ID == v2.ID &&
				v.FirstName == v2.FirstName && v.LastName == v2.LastName &&
				v.Email == v2.Email && v.LowercaseEmail == v2.LowercaseEmail &&
				!time.Time.IsZero(v2.UpdatedAt) && v.Hash == v2.Hash {
				asserted++
				break
			}
		}
	}
	if asserted != len(expAccounts) {
		t.Fatalf("asserted %d != len(expAccounts) %d\n", asserted, len(expAccounts))
	}
}

// --------------------------------
// Ingredient
func TestSaveIngredient(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"ingredient"})
	defer CloseTestDataDS()

	// create test cases
	cases := []Ingredient{
		{Name: "Carrot", RecipeID: 1, Adjective: "Large", Quantity: 2, Unit: ""},
		{Name: "Apple", RecipeID: 1, Adjective: "", Quantity: 2, Unit: "cups"},
	}
	// (0) save
	for i := range cases {
		id, err := pgDS.SaveIngredient(1, cases[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases[i].ID = id
	}

	// (1) Read all Ingredients
	dbIngredients, err := pgDS.ReadAllIngredients(1)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateIngredients(t, cases[0:], dbIngredients)
	fmt.Println("read fine")
	// (2) Update Ran
	cases[1].Adjective = "Small"
	err = pgDS.UpdateIngredient(1, cases[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	// (3) Read "Ran"
	dbIngredients, err = pgDS.ReadIngredient(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateIngredients(t, cases[1:], dbIngredients)
	fmt.Println("Readspec fine")

	dbIngredients, err = pgDS.LeaveIngredientOut(1, cases[0].ID, cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateIngredients(t, cases[2:], dbIngredients)
	// (4) Delete Ran
	pgDS.DeleteIngredient(1, cases[1].ID, cases[1].RecipeID)
	dbIngredients, err = pgDS.ReadIngredient(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbIngredients) != 0 {
		t.Fatalf("Ingredient ID=%d was not deleted properly", cases[1].ID)
	}
}

func validateIngredients(t *testing.T, expIngredients, Ingredients []Ingredient) {
	if len(expIngredients) != len(Ingredients) {
		t.Fatalf("len(expIngredients) %d != len(Ingredients) %d\n", len(expIngredients), len(Ingredients))
	}

	asserted := 0
	for _, v := range expIngredients {
		for _, v2 := range Ingredients {
			if v.ID == v2.ID &&
				v.Name == v2.Name && v.RecipeID == v2.RecipeID && v.Adjective == v2.Adjective &&
				v.Quantity == v2.Quantity && v.Unit == v2.Unit &&
				!time.Time.IsZero(v2.UpdatedAt) {
				asserted++
				break
			}
		}
	}

	if asserted != len(expIngredients) {
		t.Fatalf("asserted %d != len(expIngredients) %d\n", asserted, len(expIngredients))
	}
}

// --------------------------------
// Recipe
func TestSaveRecipe(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"recipe", "label", "recipe_label"})
	defer CloseTestDataDS()
	cases2 := []Label{
		{Name: "Drink"},
		{Name: "American"},
	}
	// (0) save
	for i := range cases2 {
		id, err := pgDS.SaveLabel(cases2[i].ID, cases2[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases2[i].ID = id
	}

	cases3 := []RecipeLabel{
		{RecipeID: 1, LabelID: 1},
		{RecipeID: 1, LabelID: 0},
	}
	// (0) save
	for i := range cases3 {
		id, err := pgDS.SaveRecipeLabel(cases3[i].ID, cases3[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases3[i].ID = id
	}
	// create test cases
	cases := []Recipe{
		{Name: "CarrotAppleDrink", AccountID: 0, Description: "a", Instruction: "Chop and put in blender"},
		{Name: "CarrotAppleDrink2", AccountID: 1, Description: "a2", Instruction: "Chop and put in blender2"},
	}
	// (0) save
	for i := range cases {
		id, err := pgDS.SaveRecipe(cases[i].AccountID, cases[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases[i].ID = id
	}

	// (1) Read all Recipes
	dbRecipes, err := pgDS.ReadAllRecipes(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateRecipes(t, cases[0:], dbRecipes)
	fmt.Println("read fine")
	// (2) Update Ran
	cases[1].Description = "Dessert"
	err = pgDS.UpdateRecipe(1, cases[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	// (3) Read "Ran"
	dbRecipes, err = pgDS.ReadRecipe(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateRecipes(t, cases[1:], dbRecipes)
	fmt.Println("Readspec fine")
	_, err = pgDS.ReadAllRecipeLabel(1)
	if err != nil {
		t.Fatalf(err.Error())
	}
	_, err = pgDS.SearchRecipe(1)
	if err != nil {
		t.Fatalf(err.Error())
	}
	// (4) Delete Ran
	pgDS.DeleteRecipe(1, cases[1].ID)
	dbRecipes, err = pgDS.ReadRecipe(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbRecipes) != 0 {
		t.Fatalf("Recipe ID=%d was not deleted properly", cases[1].ID)
	}
}

func validateRecipes(t *testing.T, expRecipes, Recipes []Recipe) {
	if len(expRecipes) != len(Recipes) {
		t.Fatalf("len(expRecipes) %d != len(Recipes) %d\n", len(expRecipes), len(Recipes))
	}

	asserted := 0
	for _, v := range expRecipes {
		for _, v2 := range Recipes {
			if v.ID == v2.ID &&
				v.Name == v2.Name && v.Description == v2.Description && //v.Label == v2.Label &&
				v.Instruction == v2.Instruction &&
				!time.Time.IsZero(v2.UpdatedAt) {
				asserted++
				break
			}
		}
	}

	if asserted != len(expRecipes) {
		t.Fatalf("asserted %d != len(expRecipes) %d\n", asserted, len(expRecipes))
	}
}

// --------------------------------
// Label
func TestSaveLabel(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"label"})
	defer CloseTestDataDS()

	cases := []Label{
		{Name: "Drink"},
		{Name: "American"},
	}
	// (0) save
	for i := range cases {
		id, err := pgDS.SaveLabel(cases[i].ID, cases[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases[i].ID = id
	}
	// (1) Read all Labels
	dbLabels, err := pgDS.ReadAllLabels(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateLabels(t, cases[0:], dbLabels)
	fmt.Println("read fine")
	// (2) Update Ran
	cases[1].Name = "British"
	err = pgDS.UpdateLabel(cases[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	// (3) Read "Ran"
	dbLabels, err = pgDS.ReadLabel(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateLabels(t, cases[1:], dbLabels)

	// (4) Delete Ran
	pgDS.DeleteLabel(1, cases[1].ID)
	dbLabels, err = pgDS.ReadLabel(cases[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbLabels) != 0 {
		t.Fatalf("Label ID=%d was not deleted properly", cases[1].ID)
	}
}

func validateLabels(t *testing.T, expLabels, Labels []Label) {
	if len(expLabels) != len(Labels) {
		t.Fatalf("len(expLabels) %d != len(Labels) %d\n", len(expLabels), len(Labels))
	}

	asserted := 0
	for _, v := range expLabels {
		for _, v2 := range Labels {
			if v.ID == v2.ID &&
				v.Name == v2.Name {
				asserted++
				break
			}
		}
	}

	if asserted != len(expLabels) {
		t.Fatalf("asserted %d != len(expLabels) %d\n", asserted, len(expLabels))
	}
}

// --------------------------------
// RecipeLabel
func TestSaveRecipeLabel(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"recipe_label"})
	defer CloseTestDataDS()

	cases3 := []RecipeLabel{
		{RecipeID: 1, LabelID: 1},
		{RecipeID: 1, LabelID: 0},
	}
	// (0) save
	for i := range cases3 {
		id, err := pgDS.SaveRecipeLabel(cases3[i].ID, cases3[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases3[i].ID = id
	}
	// (1) Read all RecipeLabels
	dbRecipeLabels, err := pgDS.ReadAllRecipeLabels(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateRecipeLabels(t, cases3[0:], dbRecipeLabels)
	fmt.Println("read fine")
	// (2) Update Ran
	cases3[1].RecipeID = 2
	err = pgDS.UpdateRecipeLabel(cases3[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	// (3) Read "Ran"
	dbRecipeLabels, err = pgDS.ReadRecipeLabel(cases3[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateRecipeLabels(t, cases3[1:], dbRecipeLabels)

	// (4) Delete Ran
	pgDS.DeleteRecipeLabel(1, cases3[1].ID)
	dbRecipeLabels, err = pgDS.ReadRecipeLabel(cases3[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbRecipeLabels) != 0 {
		t.Fatalf("RecipeLabel ID=%d was not deleted properly", cases3[1].ID)
	}
}

func validateRecipeLabels(t *testing.T, expRecipeLabels, RecipeLabels []RecipeLabel) {
	if len(expRecipeLabels) != len(RecipeLabels) {
		t.Fatalf("len(expRecipeLabels) %d != len(RecipeLabels) %d\n", len(expRecipeLabels), len(RecipeLabels))
	}

	asserted := 0
	for _, v := range expRecipeLabels {
		for _, v2 := range RecipeLabels {
			if v.ID == v2.ID &&
				v.Name == v2.Name && v.RecipeID == v2.RecipeID &&
				v.LabelID == v2.LabelID {
				asserted++
				break
			}
		}
	}

	if asserted != len(expRecipeLabels) {
		t.Fatalf("asserted %d != len(expRecipeLabels) %d\n", asserted, len(expRecipeLabels))
	}
}

// --------------------------------
// Day
func TestSaveDay(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"recipe", "day", "day_recipe"})
	defer CloseTestDataDS()
	// create test cases
	cases := []Recipe{
		{Name: "CarrotAppleDrink", AccountID: 0, Description: "a", Instruction: "Chop and put in blender"},
		{Name: "CarrotAppleDrink2", AccountID: 1, Description: "a2", Instruction: "Chop and put in blender2"},
	}
	// (0) save
	for i := range cases {
		id, err := pgDS.SaveRecipe(cases[i].AccountID, cases[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases[i].ID = id
	}

	cases2 := []DayRecipe{
		{DayID: 1, RecipeID: 1},
		{DayID: 1, RecipeID: 2},
	}
	// (0) save
	for i := range cases2 {
		id, err := pgDS.SaveDayRecipe(cases2[i].ID, cases2[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases2[i].ID = id
	}
	cases3 := []Day{
		{Week: 1, Daynum: 1, WeekDay: "11"},
		{Week: 1, Daynum: 2, WeekDay: "12"},
	}

	// (0) save
	for i := range cases3 {
		id, err := pgDS.SaveDay(cases3[i].ID, cases3[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases3[i].ID = id
	}
	// (1) Read all Days
	dbDays, err := pgDS.ReadAllDays(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateDays(t, cases3[0:], dbDays)
	fmt.Println("read fine")
	// (2) Update Ran
	cases3[1].Daynum = 2
	err = pgDS.UpdateDay(cases3[1])
	if err != nil {
		t.Fatalf(err.Error())
	}

	dbRecipe, err := pgDS.ReadDayRecipes(1)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateRecipes(t, cases[0:], dbRecipe)
	// (3) Read "Ran"
	dbDays, err = pgDS.ReadDay(cases3[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateDays(t, cases3[1:], dbDays)

	// (4) Delete Ran
	pgDS.DeleteDay(1, cases3[1].ID)
	dbDays, err = pgDS.ReadDay(cases3[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbDays) != 0 {
		t.Fatalf("Day ID=%d was not deleted properly", cases3[1].ID)
	}
}

func validateDays(t *testing.T, expDays, Days []Day) {
	if len(expDays) != len(Days) {
		t.Fatalf("len(expDays) %d != len(Days) %d\n", len(expDays), len(Days))
	}

	asserted := 0
	for _, v := range expDays {
		for _, v2 := range Days {
			if v.ID == v2.ID &&
				v.Week == v2.Week && v.Daynum == v2.Daynum &&
				v.WeekDay == v2.WeekDay {
				asserted++
				break
			}
		}
	}

	if asserted != len(expDays) {
		t.Fatalf("asserted %d != len(expDays) %d\n", asserted, len(expDays))
	}
}

// --------------------------------
// Day
func TestSaveDayRecipe(t *testing.T) {
	// initialize the config files and set websocket.enabled to false
	utils.InitConfig([]string{"../../recipefiles/recipe-test.json", "../../recipefiles/recipe.json"})

	SetupTestDataDS("../../testfiles/devdb.db", []string{"day_recipe"})
	defer CloseTestDataDS()
	// create test cases

	cases2 := []DayRecipe{
		{DayID: 1, RecipeID: 1},
		{DayID: 1, RecipeID: 2},
	}
	// (0) save
	for i := range cases2 {
		id, err := pgDS.SaveDayRecipe(cases2[i].ID, cases2[i])
		if err != nil {
			t.Fatalf("error %+v\n", err)
		}
		cases2[i].ID = id
	}

	// (1) Read all DayRecipes
	dbDayRecipes, err := pgDS.ReadAllDayRecipes(2)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateDayRecipes(t, cases2[0:], dbDayRecipes)
	fmt.Println("read fine")
	// (2) Update Ran
	cases2[1].RecipeID = 2
	err = pgDS.UpdateDayRecipe(cases2[1])
	if err != nil {
		t.Fatalf(err.Error())
	}
	// (3) Read "Ran"
	dbDayRecipes, err = pgDS.ReadDayRecipe(cases2[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	validateDayRecipes(t, cases2[1:], dbDayRecipes)

	// (4) Delete Ran
	pgDS.DeleteDayRecipe(1, cases2[1].ID)
	dbDayRecipes, err = pgDS.ReadDayRecipe(cases2[1].ID)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if len(dbDayRecipes) != 0 {
		t.Fatalf("DayRecipe ID=%d was not deleted properly", cases2[1].ID)
	}
}

func validateDayRecipes(t *testing.T, expDayRecipes, DayRecipes []DayRecipe) {
	if len(expDayRecipes) != len(DayRecipes) {
		t.Fatalf("len(expDayRecipes) %d != len(DayRecipes) %d\n", len(expDayRecipes), len(DayRecipes))
	}

	asserted := 0
	for _, v := range expDayRecipes {
		for _, v2 := range DayRecipes {
			if v.ID == v2.ID &&
				v.RecipeID == v2.RecipeID && v.DayID == v2.DayID {
				asserted++
				break
			}
		}
	}

	if asserted != len(expDayRecipes) {
		t.Fatalf("asserted %d != len(expDayRecipes) %d\n", asserted, len(expDayRecipes))
	}
}
