# Build and deploy bujo to Kubernetes

## Create a EKS Fargate Profile for prod and stage

```bash
eksctl create fargateprofile --cluster bujo-fargate --region us-west-2 --name prod --namespace prod
[ℹ]  creating Fargate profile "prod" on EKS cluster "bujo-fargate"
[ℹ]  created Fargate profile "prod" on EKS cluster "bujo-fargate"
```

## Create and Get Secrets in and from Kubernetes

```bash
# Create secrets
kubectl create -n prod secret generic bujo.prod --from-literal=db.user=devuser --from-literal=db.password=devpassword --from-literal=db.host=localhost

# Get secrets
kubectl get -n prod secrets bujo.prod -o yaml

# Decode secrets
echo 'c2t1by1wZy5jdzRpY3p4c2R6YnYudXMtd2VzdC0yLnJkcy5hbWF6b25hd3MuY29t' |base64 --decode

# Delete secrets
kubectl delete -n prod secret bujo.prod
```

## Go Docker Container Deployed to EKS and Postgres RDS  
  
```bash  
# --------------------------------------
# Prep work.  The following commands were tested on Steve's Mac
cd ~/.kube
./kubeConfig bujo-fargate
kubectl -n prod get pods
# to get pods in all namespaces
kubectl get pods --all-namespaces
# login to prod and make sure it is functioning correctly
# sign in to AWS ECR
# connect to prod's RDS in pgadmin

# find bujo docker images
docker images |grep -w "bujo"
# delete old bujo docker images
docker rmi <image-hash>

# --------------------------------------
# be sure to build the release branch
go clean; go build
gotest

# Build Bujo Docker Container  
./buildDocker prod release-20200131-b
# test built docker on local mac first
./deployDocker local release-20200131-b
# push to ECR or Artifactory  
./pushDocker ecr release-20200131-b
# liquibase update
liquibase/liquibase --username=bujoprod --password=${DB_PASSWORD} --defaultsFile liquibase/liquibase-prod.properties update
# execution migration scripts 
./scripts/migration/run.sh
# rolling update pods only
./rollingUpdatePods prod release-20200131-b

# --------------------------------------
# mv ./scripts/migration/* ./scripts/migration/archived/.
# update master.changelog.xml
# merge the release branch to master and develop

# --------------------------------------
# delete deployment
kubectl delete -n prod deployment bujo-prod
# create deployment
kubectl create -f aws/k8s/bujo-deploy-prod.yaml

# deploy to eks: depoyment, service and ingress
./deployDocker prod
# delete bujo-prod
./deployDocker prod delete  

# --------------------------------------
# watch the log of a pod  
kubectl -n prod logs -f <pod-id>  
# login to a pod
kubectl -n proj exec -it bujo-prod-5ff8448fdc-bnh9j /bin/bash

# --------------------------------------
# cp from a pod
kubectl -n prod cp <pod-id>:/data/bujo/tmp/114/dd.csv .
# cp to a pod
kubectl -n prod cp /data/bujo/tmp/536 <pod-id>:/data/bujo/tmp/91/

# -------------------------------------
# debug pods, e.g. stuck in the 'pending' state
kubectl describe -n prod node -o wide

# --------------------------------------
# find all the images in a cluster  
kubectl get pods --all-namespaces -o jsonpath="{..image}" | tr -s '[[:space:]]' '\n' | sort | uniq -c
```  

## Push Docker Image  
  
```bash  
# push to ECR 
./pushDocker ecr bujo:prod-SM-189    
```  
  
## Rolling Update Kubernetes Pods  
  
```bash  
# rolling update  
./rollingUpdatePods prod prod-SM-208  
```  
