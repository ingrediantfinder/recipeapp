# AWS RDS Postgres

## Configuration for bujo-prod in the eksclt-bujo VPC

https://us-west-2.console.aws.amazon.com/rds/home?region=us-west-2#database:id=bujo-prod;is-cluster=false

Create a RDS instance for Posgres on the AWS console with the following configurations.

- DBInstanceIdentifier: bujo-prod
- MasterUsername: bujo
- MasterUserpassword: <see LastPass>
- Address: bujo-prod.cun8dtuc9ebe.us-west-2.rds.amazonaws.com
- Port: 5432
- Public accessibility: Yes

## Update Its Security Group to Accept Inbound Traffic from my IP Address

Type        Protocol    Port range          Source              Description - optional
PostgreSQL	TCP	        5432	            104.34.122.4/32	    -

## Update Its Security Group to Accept Inbound Traffic from Fargate Nodes

Type        Protocol    Port range          Source              Description - optional
PostgreSQL	TCP	        5432	            192.168.0.0/16	    Allowing EKS fargate nodes to acces RDS

## Create User bujoprod

Login to the created rds (e.g. bujo-prod) and execute the queries in sql/initDBProps.sql.  Remember to 
replace `devuser` and `devpassword` with `bujo-prod` and its password.

## Find RDS description

```bash
aws rds describe-db-instances
```
