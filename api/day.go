package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveDay implements endpoint POST /api/v1/day
func SaveDay(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var day db.Day
	err = json.Unmarshal(body, &day)
	if err != nil {
		utils.LogErrorf("Error unmarshal day: %v", err)
		http.Error(w, "can't unmarshal day", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveDay(ID, day)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadIngredient implements endpoint GET /api/v1/Day/{DayID}
func ReadDay(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadDay"

	idParam := request.PathParam("dayID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadIngredient(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadIngredient implements endpoint GET /api/v1/Day/{DayID}
func ReadDayRecipes(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadDayRecipes"

	idParam := request.PathParam("dayID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadDayRecipes(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllDays implements endpoint GET /api/v1/day
func ReadAllDays(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllDays"
	// Get all days
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllDays(ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Days: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateDay implements endpoint PUT /api/v1/day/{day}
func UpdateDay(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateDay"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var day db.Day
	err = json.Unmarshal(body, &day)
	if err != nil {
		utils.LogErrorf("Error unmarshal day: %v", err)
		http.Error(w, "can't unmarshal day", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("dayID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	//var ID = tk.AccountID
	err = db.Manager.DS.UpdateDay(day)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteDay  implements endpoint DEL /api/v1/day/{day}
func DeleteDay(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteDay"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	idParam := request.PathParam("dayID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err = db.Manager.DS.DeleteDay(ID, id)
	if err != nil {
		utils.LogError("Failed to delete day")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
