select id, author, filename, orderexecuted, tag from databasechangelog order by orderexecuted desc;
update databasechangelog set tag='Recipe-' where id = 'BUJO-6.0';
delete from databasechangelog where id like 'Recipe-4';
delete from databasechangelog;
ALTER SEQUENCE task_id_seq RESTART 1000;
delete from ingredient;
select * from account;
select * from ingredient;
select * from recipe;
select * from priority;
select * from recipe_image;
INSERT INTO recipe_image (recipe_id, image_url, updated_at) 
VALUES (207, '/images/1c778f90-337a-459a-b8d5-c6e2a0fddb88.png', NOW());
delete from recipe_image where id = 8;

SELECT t.id, t.name, t.status, t.account_id, t.kind, t.labels, 
t.startdate, t.enddate, t.updated_at, t.created_at, 
p.id, p.name, p.priority
FROM task t JOIN priority p ON t.priority_id = p.id
ORDER by t.id;

UPDATE task set priority_id = 1;