package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveShopping_list_ingredient implements endpoint POST /api/v1/Shopping_list_ingredient
func SaveShopping_list_ingredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.SaveShopping_list_ingredient"

	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		// utils.Error() called by request.ParseParams
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	rid := idParam.AsInt32()
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	//var ID int32 = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var Shopping_list_ingredient db.Shopping_list_ingredient
	fmt.Println(Shopping_list_ingredient.Quantity)
	err = json.Unmarshal(body, &Shopping_list_ingredient)
	if err != nil {
		utils.LogErrorf("Error unmarshal Shopping_list_ingredient: %v", err)
		http.Error(w, "can't unmarshal Shopping_list_ingredient", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveShopping_list_ingredient(rid, Shopping_list_ingredient)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadShopping_list_ingredient implements endpoint GET /api/v1/Shopping_list_ingredient/{Shopping_list_ingredient}
func ReadShopping_list_ingredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadShopping_list_ingredient"

	// Get Shopping_list_ingredient
	results, err := db.Manager.DS.ReadShopping_list_ingredient(1)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Shopping_list_ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllShopping_list_ingredients implements endpoint GET /api/v1/Shopping_list_ingredient
func ReadAllShopping_list_ingredients(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllShopping_list_ingredients"
	// Get all Shopping_list_ingredients
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	idParam := request.PathParam("shopping_list_ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	results, err := db.Manager.DS.ReadAllShopping_list_ingredients(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Shopping_list_ingredients: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateShopping_list_ingredient implements endpoint PUT /api/v1/Shopping_list_ingredient/{Shopping_list_ingredient}
func UpdateShopping_list_ingredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateShopping_list_ingredient"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var Shopping_list_ingredient db.Shopping_list_ingredient
	err = json.Unmarshal(body, &Shopping_list_ingredient)
	if err != nil {
		utils.LogErrorf("Error unmarshal Shopping_list_ingredient: %v", err)
		http.Error(w, "can't unmarshal Shopping_list_ingredient", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("shopping_list_ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()
	Shopping_list_ingredient.ID = idParam.AsInt32()
	err = db.Manager.DS.UpdateShopping_list_ingredient(Shopping_list_ingredient)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteShopping_list_ingredient  implements endpoint DEL /api/v1/Shopping_list_ingredient/{Shopping_list_ingredient}
func DeleteShopping_list_ingredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteShopping_list_ingredient"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	idParam := request.PathParam("shopping_list_ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	err = db.Manager.DS.DeleteShopping_list_ingredient(1, id)
	if err != nil {
		utils.LogError("Failed to delete Shopping_list_ingredient")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}

// ReadAllShopping_list_ingredients implements endpoint GET /api/v1/Shopping_list_ingredient
func ReadIngredientTotals(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllShopping_list_ingredients"
	// Get all Shopping_list_ingredients
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	idParam := request.PathParam("shopping_list_ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	results, err := db.Manager.DS.ReadIngredientTotals(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Shopping_list_ingredients: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}
