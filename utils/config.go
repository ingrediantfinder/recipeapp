package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/tidwall/gjson"
)

var jsonStrs []string

// Initialize reads env variable DEPLOY_ENV and if it exists, set deployEnv.  It also calls
// InitConfig passing the correct files.
func Initialize(currDir string) {
	files := []string{}
	// if deployEnv != "", append "currDir/smfiles/sm-DEPLOY_ENV.json" file
	if os.Getenv("DEPLOY_ENV") != "" {
		files = append(files, fmt.Sprintf("%s/recipefiles/recipe-%s.json", currDir, os.Getenv("DEPLOY_ENV")))
	}
	files = append(files, fmt.Sprintf("%s/recipefiles/recipe.json", currDir))
	fmt.Printf("DEPLOY_ENV=%s\n", os.Getenv("DEPLOY_ENV"))
	fmt.Printf("configFiles=%v\n", files)
	InitConfig(files)
}

// InitConfig sets a slice of JSON files that will be searched sequentially.  The first one wins.
func InitConfig(files []string) {
	// reset jsonStrs
	jsonStrs = make([]string, 0)
	for _, file := range files {
		// read and set overrider JSON string one by one
		data, err := ioutil.ReadFile(file)
		CheckErr(err)
		jsonStr := string(data)
		jsonStrs = append(jsonStrs, jsonStr)
	}
}

// GetConfig returns gjson.Result for the input path
func GetConfig(path string) gjson.Result {
	var result gjson.Result
	// Loop through the jsonStrs
	for _, jsonStr := range jsonStrs {
		result := gjson.Get(jsonStr, path)
		if result.Exists() {
			return result
		}
	}
	return result
}

// GetSecret returns the input env variable if it is found; otherwise it calls GetConfig to return
// gjson.Result
func GetSecret(path string) gjson.Result {
	if os.Getenv(path) != "" {
		num, err := strconv.ParseFloat(os.Getenv(path), 64)
		if err == nil {
			return gjson.Result{Type: gjson.Number, Num: num}
		}
		return gjson.Result{Type: gjson.String, Str: os.Getenv(path)}
	}
	return GetConfig(path)
}
