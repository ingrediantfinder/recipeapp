-- liquibase formatted sql

-- changeset skuo:liquibase_test.0
-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS liquibase_test (
    id serial NOT NULL,
    name character varying(128) NOT NULL
);
-- rollback drop table liquibase_test;

-- changeset skuo:liquibase_test.1
insert into liquibase_test (id, name) values(1, 'name 1');
insert into liquibase_test (id, name) values(2, 'name 2');
-- rollback delete from liquibase_test where name in ('name 1', 'name 2');

-- changeset skuo:liquibase_test.2
UPDATE liquibase_test set name = 'name 1 updated' where id = 1;
delete from liquibase_test where id = 1;
insert into liquibase_test (id, name) values(1, 'name 1');
-- rollback delete from liquibase_test where name in ('name 1 updated', 'set up to fail');
