# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/google/uuid v1.3.0
## explicit
github.com/google/uuid
# github.com/gorilla/context v1.1.1
## explicit
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
github.com/gorilla/mux
# github.com/gorilla/securecookie v1.1.1
## explicit
github.com/gorilla/securecookie
# github.com/gorilla/websocket v1.4.2
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/lib/pq v1.10.2
## explicit; go 1.13
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/mattn/go-sqlite3 v1.14.8
## explicit; go 1.12
github.com/mattn/go-sqlite3
# github.com/tidwall/gjson v1.8.1
## explicit; go 1.12
github.com/tidwall/gjson
# github.com/tidwall/match v1.0.3
## explicit; go 1.15
github.com/tidwall/match
# github.com/tidwall/pretty v1.1.0
## explicit
github.com/tidwall/pretty
# github.com/tidwall/sjson v1.1.7
## explicit; go 1.14
github.com/tidwall/sjson
# golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
## explicit; go 1.17
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
