package utils

import (
	"RecipeApp/constant"
	"errors"
	"fmt"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// Lazily instantiated in GetDataBasePath()
var dataBasePath string

// GetDataBasePath is a function to get the absolute base directory for data
// file storage.  Used for all file uploads and for DS file results.
func GetDataBasePath() string {
	if dataBasePath == "" {
		// Lazy instantiation.  Try to get env or config value, otherwise use default
		dataBasePath = GetSecret("data.basepath").Str
		if dataBasePath == "" {
			var err error
			// attempt to assign to absolute current working directory
			dataBasePath, err = os.Getwd()
			if err != nil {
				LogError("Problem getting absolute base data dir.  Using './': " + err.Error())
				// The 'I give up' assignment
				dataBasePath = "./"
			}
		}
		if !strings.HasSuffix(dataBasePath, "/") {
			// Ensure base path ends with '/'
			dataBasePath += "/"
		}
	}
	return dataBasePath
}

// CheckErr panics if err is not nil
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// FormatInClause returns an in clause for input []string
func FormatInClause(strs []string) string {
	inClause := make([]string, 0)
	for _, str := range strs {
		for _, s := range strings.Split(str, ",") {
			inClause = append(inClause, fmt.Sprintf("'%s'", s))
		}
	}
	return strings.Join(inClause, ",")
}

// FormatDate returns input date in yyyy-mm-dd format
func FormatDate(date time.Time) string {
	return fmt.Sprintf("%04d-%02d-%02d", date.Year(), date.Month(), date.Day())
}

// FormatTimestamp returns input date in yyyy-mm-dd HH:MM:SS format
func FormatTimestamp(timestamp time.Time) string {
	return timestamp.Format(time.RFC3339)
	//return fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", timestamp.Year(), timestamp.Month(), timestamp.Day(),
	//	timestamp.Hour(), timestamp.Minute(), timestamp.Second())
}

// FormatTimestampAsFilename returns input date in yyyymmddHHMMSS format
func FormatTimestampAsFilename(timestamp time.Time) string {
	return fmt.Sprintf("%04d%02d%02d%02d%02d%02d", timestamp.Year(), timestamp.Month(), timestamp.Day(),
		timestamp.Hour(), timestamp.Minute(), timestamp.Second())
}

// TimestampToDate returns a new time.Time created from input time.Time's Year, Month and Day
func TimestampToDate(timestamp time.Time) time.Time {
	return time.Date(timestamp.Year(), timestamp.Month(), timestamp.Day(),
		0, 0, 0, 0, time.UTC)
}

// StrToTime catch-all string -> date-time.  First tries RFC339Nano parsing, if fail,
// tries csv data science format.  Returns empty time on err
func StrToTime(str string) time.Time {
	t, err := time.Parse(time.RFC3339Nano, str)
	if err != nil {
		t, err = time.Parse(constant.CSVTimeFormat, str)
		if err != nil {
			LogErrorf("[utils.StrToTime()] Erorr parsing time: %+v", err)
			return t
		}
		t = t.UTC()
	}
	return t
}

// ToFloat64P returns nil for Nan, else return &f. Useful in JSON marshalling.
func ToFloat64P(f float64) *float64 {
	if math.IsNaN(f) {
		return nil
	}
	return &f
}

// FromFloat64P returns Nan for nil, else returns *f. Useful in JSON unmarshalling.
func FromFloat64P(f *float64) float64 {
	if f == nil {
		return math.NaN()
	}
	return *f
}

// CmpFloat64 returns true if both f1 and f2 are NaN, else it returns f1 == f2
func CmpFloat64(f1, f2 float64) bool {
	if math.IsNaN(f1) && math.IsNaN(f2) {
		return true
	}
	return f1 == f2
}

// CmpInt32P returns true if (1) both ip1 and ip1 are nil or (2) ip1 and ip2 are not nil and *ip1 == *ip2
func CmpInt32P(ip1, ip2 *int32) bool {
	if ip1 == nil && ip2 == nil {
		return true
	}
	if ip1 != nil && ip2 != nil {
		return *ip1 == *ip2
	}
	return false
}

// CmpFloat64P returns true if both fp1 and fp2 are nil or (fp1 != nil) and *fp1 == *fp2.  Else it returns false
func CmpFloat64P(fp1, fp2 *float64, tolerance float64) bool {
	if fp1 == nil && fp2 == nil {
		return true
	} else if fp1 != nil && fp2 != nil {
		return math.Abs(*fp1-*fp2) <= tolerance
	}
	return false
}

// CmpStrP returns true if (1) both sp1 and sp1 are nil or (2) sp1 and sp2 are not nil and *sp1 == *sp2
func CmpStrP(sp1, sp2 *string) bool {
	if sp1 == nil && sp2 == nil {
		return true
	}
	if sp1 != nil && sp2 != nil {
		return *sp1 == *sp2
	}
	return false
}

// CmpTimeP returns true if (1) both tp1 and tp1 are nil or (2) tp1 and tp2 are not nil and *tp1 == *tp2
func CmpTimeP(tp1, tp2 *time.Time) bool {
	if tp1 == nil && tp2 == nil {
		return true
	}
	if tp1 != nil && tp2 != nil {
		return (*tp1).Equal(*tp2)
	}
	return false
}

// ToTimeP returns nil for Zero value time.Time, else returns &t. Useful in JSON marshalling.
func ToTimeP(t time.Time) *time.Time {
	if t.IsZero() {
		return nil
	}
	return &t
}

// FromTimeP returns Zero value time.Time, else *t. Useful in JSON unmarshalling.
func FromTimeP(t *time.Time) time.Time {
	if t == nil {
		return time.Time{}
	}
	return *t
}

// Int32POrDefault returns the input ip if it is not nil.  Otherwise, it returns the pointer of the input i
func Int32POrDefault(ip *int32, i int32) *int32 {
	if ip == nil {
		return ToInt32P(i)
	}
	return ip
}

// Int32PToFloat64P converts a int32 pointer to float64 pointer
func Int32PToFloat64P(ip *int32) *float64 {
	if ip == nil {
		return nil
	}
	return ToFloat64P(float64(*ip))
}

// Float64POrDefault returns the input fp if it is not nil.  Otherwise, it returns the pointer of the first non-NaN input f
func Float64POrDefault(fp *float64, fs ...float64) *float64 {
	if fp == nil {
		for _, f := range fs {
			if !math.IsNaN(f) {
				return ToFloat64P(f)
			}
		}
	}
	return fp
}

// Float64OrDefault returns the input float if it is not Nan.  Otherwise, it returns the first non-Nan input f
func Float64OrDefault(f0 float64, fs ...float64) float64 {
	if math.IsNaN(f0) {
		for _, f := range fs {
			if !math.IsNaN(f) {
				return f
			}
		}
	}
	return f0
}

// Float64PToString converts a *float64 into a string.  A nil pointer is converted into "null"
func Float64PToString(fp *float64) string {
	if fp == nil {
		return "null"
	}
	return fmt.Sprintf("%f", *fp)
}

// Substring returns the first n string if n <= len(str).  Otherwise, the original string is returned.
func Substring(str string, n int) string {
	if len(str) > n {
		return str[:n]
	}
	return str
}

// StringToFloat64 converts a string to a float64.  "null" is converted to NaN
// If defaultValue is provided, it will be returned on null or empty. Otherwise NaN will be returned.
func StringToFloat64(str string, defaultValue ...float64) (float64, error) {
	defaultRet := math.NaN()
	if len(defaultValue) > 0 {
		defaultRet = defaultValue[0]
	}
	if str == "" || strings.ToLower(str) == "null" {
		return defaultRet, nil
	}
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return defaultRet, err
	}
	return f, nil
}

// StringToFloat64P converts a string to a *float64.  "null" is converted to nil
func StringToFloat64P(str string) (*float64, error) {
	if str == "" || strings.ToLower(str) == "null" {
		return nil, nil
	}
	f, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return nil, err
	}
	return &f, nil
}

// StringToBoolP converts a string to a *bool.
func StringToBoolP(str string) (*bool, error) {
	if str == "" {
		return nil, nil
	}
	b, err := strconv.ParseBool(str)
	if err != nil {
		return nil, err
	}
	return &b, nil
}

// StringOrNull returns either the given string with single quotes '' if not empty, or the string
// "null" by itself for db inserts
func StringOrNull(str string) string {
	if str == "" {
		return "null"
	}
	return fmt.Sprint("'", str, "'")
}

// Int32PToString returns either the given number as a string if not zero, or the
// string "null" for db inserts
func Int32PToString(num *int32) string {
	if num == nil {
		return "null"
	}
	return fmt.Sprint(*num)
}

// ToInt32P returns &i. Useful in JSON marshalling.
func ToInt32P(i int32) *int32 {
	return &i
}

// Int64PToString returns either the given number as a string if not zero, or the
// string "null" for db inserts
func Int64PToString(num *int64) string {
	if num == nil {
		return "null"
	}
	return fmt.Sprint(*num)
}

// ToInt64P returns &i. Useful in JSON marshalling.
func ToInt64P(i int64) *int64 {
	return &i
}

/*
ForEach helper function to iterate an array/slice of interface types generically.
arr: the array or slice to iterate.  Can be any type.
forEachFunc: function with the signature (i int, value <Interface>) bool, where <Interface>
is a discrete type.  Built in or created.  Elements of the array/slice must be assignable
to the value type.
*/
func ForEach(arr interface{}, forEachFunc interface{}) error {
	arrValue := reflect.ValueOf(arr)
	funcValue := reflect.ValueOf(forEachFunc)
	funcType := funcValue.Type()

	if (arrValue.Kind() == reflect.Array || arrValue.Kind() == reflect.Slice) && arrValue.Len() > 0 {
		arrElemType := arrValue.Type().Elem()
		if funcType.Kind() == reflect.Func && funcType.NumIn() == 2 && funcType.In(0).Kind() == reflect.Int {
			if arrElemType.ConvertibleTo(funcType.In(1)) {
				for i := 0; i < arrValue.Len(); i++ {
					if !funcValue.Call([]reflect.Value{reflect.ValueOf(i), arrValue.Index(i)})[0].Bool() {
						break
					}
				}
			} else {
				return errors.New("forEachFunc value parameter cannot accept the array/slice value types")
			}
		} else {
			return errors.New("forEachFunc does not have appropriate signature.  See documentation")
		}
	} else {
		return errors.New("arr must be an array or slice")
	}
	return nil
}

// ArrToStr prints array values to a string with the given deliminator.  Just the contents are printed.
func ArrToStr(arr interface{}, delim string) string {
	arrValue := reflect.ValueOf(arr)
	var ret string
	if (arrValue.Kind() == reflect.Array || arrValue.Kind() == reflect.Slice) && arrValue.Len() > 0 {
		for i := 0; i < arrValue.Len(); i++ {
			ret += fmt.Sprintf("%v", arrValue.Index(i))
			if i != arrValue.Len()-1 {
				ret += delim
			}
		}
	}
	return ret
}

// MinMaxFloat64 returns the min and max float64 values for the input slice
func MinMaxFloat64(slice []float64) (float64, float64) {
	var min, max float64
	for i, e := range slice {
		if i == 0 || e < min {
			min = e
		}
		if i == 0 || e > max {
			max = e
		}
	}
	return min, max
}

// IsNil checks any value to see if it's one of go's nil types (cuz nil is a
// type and some kinds built-ins have individual nil bits)
func IsNil(thing interface{}) bool {
	if thing == nil {
		return true
	}
	value := reflect.ValueOf(thing)
	//chan, func, interface, map, pointer, or slice
	switch value.Kind() {
	case reflect.Chan, reflect.Func, reflect.Interface, reflect.Map, reflect.Ptr, reflect.Slice:
		if value.IsNil() {
			return true
		}
	}
	return false
}

// FloatToBool float to bool
func FloatToBool(value float64) bool {
	return value > 0
}

// BoolToFloat bool to float
func BoolToFloat(value bool) float64 {
	if value {
		return 1.0
	}
	return 0
}

// EqualsEnoughP is just EqualsEnough with float pointers
func EqualsEnoughP(val1, val2 *float64, precision int) bool {
	if val1 != nil && val2 != nil {
		return EqualsEnough(*val1, *val2, precision)
	}
	// returning true if both values are nil
	return true
}

// EqualsEnough compares 2 float values to the given precision.  Useful when you
// need to find numbers but minor precision resolution gets in the way.  Zero
// precision just does an integer comparison.
func EqualsEnough(val1, val2 float64, precision int) bool {
	if precision >= 0 {
		multiplier := math.Pow(10, float64(precision))
		return math.Round(val1*multiplier) == math.Round(val2*multiplier)
	}
	return false
}

// EscapeSQLSingleQuote replace ' in a string with '' so that it can be saved to db
func EscapeSQLSingleQuote(s string) string {
	return strings.Replace(s, "'", "''", -1)
}

// MkdirAll is needed because on an unix system, the perf may be filtered.
// Reference https://github.com/golang/go/issues/15210 for more detail.
// The solution is to create the directory and then chmod explicitly
func MkdirAll(path string, perm os.FileMode) error {
	err := os.MkdirAll(path, perm)
	if err != nil {
		return err
	}
	err = os.Chmod(path, perm)
	if err != nil {
		return err
	}
	return nil
}
