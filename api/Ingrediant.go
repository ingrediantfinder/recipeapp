package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveIngredient implements endpoint POST /api/v1/Ingredient
func SaveIngredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.SaveIngredient"

	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		// utils.Error() called by request.ParseParams
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	rid := idParam.AsInt32()
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	//var ID int32 = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var Ingredient db.Ingredient
	fmt.Println(Ingredient.RecipeID)
	fmt.Println(Ingredient.Quantity)
	err = json.Unmarshal(body, &Ingredient)
	if err != nil {
		utils.LogErrorf("Error unmarshal Ingredient: %v", err)
		http.Error(w, "can't unmarshal Ingredient", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveIngredient(rid, Ingredient)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadIngredient implements endpoint GET /api/v1/Ingredient/{Ingredient}
func ReadIngredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadIngredient"

	idParam := request.PathParam("ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadIngredient(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadIngredient implements endpoint GET /api/v1/Ingredient/{Ingredient}
func LeaveIngredientOut(w http.ResponseWriter, req *http.Request) {
	funcName := "api.LeaveIngredientOut"

	idParam := request.PathParam("ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	idParam2 := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id2 := idParam2.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.LeaveIngredientOut(id2, id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllIngredients implements endpoint GET /api/v1/Ingredient
func ReadAllIngredients(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllIngredients"
	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	// Get all Ingredients
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	//var ID int32 = tk.AccountID

	//utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllIngredients(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredients: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllIngredients implements endpoint GET /api/v1/Ingredient
func ReadRecipeFromIngredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadRecipeFromIngredient"
	idParam := request.PathParam("ingredientID", reflect.String)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsString()
	id = strings.ToLower(id)
	id = strings.Title(id)
	// Get all Ingredients
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	//var ID int32 = tk.AccountID

	//utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadRecipeFromIngredient(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredients: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateIngredient implements endpoint PUT /api/v1/Ingredient/{Ingredient}
func UpdateIngredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateIngredient"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var Ingredient db.Ingredient
	err = json.Unmarshal(body, &Ingredient)
	if err != nil {
		utils.LogErrorf("Error unmarshal Ingredient: %v", err)
		http.Error(w, "can't unmarshal Ingredient", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("ingredientID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()
	Ingredient.ID = idParam.AsInt32()
	idParam2 := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id2 := idParam2.AsInt32()
	err = db.Manager.DS.UpdateIngredient(id2, Ingredient)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteIngredient  implements endpoint DEL /api/v1/Ingredient/{Ingredient}
func DeleteIngredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteIngredient"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32 = tk.AccountID
	idParam := request.PathParam("ingredientID", reflect.String)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsString()
	id = strings.ToLower(id)
	id = strings.Title(id)
	idParam2 := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam2) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id2 := idParam2.AsInt32()
	err = db.Manager.DS.DeleteIngredient(ID, id, id2)
	if err != nil {
		utils.LogError("Failed to delete Ingredient")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
