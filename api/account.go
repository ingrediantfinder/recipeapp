package api

import (
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
)

// SaveAccount implements endpoint POST /api/v1/account
func SaveAccount(w http.ResponseWriter, req *http.Request) {
	// Read req's body and convert []byte to string

	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var account db.Account
	err = json.Unmarshal(body, &account)
	if err != nil {
		utils.LogErrorf("Error unmarshal account: %v", err)
		http.Error(w, "can't unmarshal account", http.StatusBadRequest)
		return
	}
	// generate hashed, salted password
	hash, err := utils.HashAndSalt(account.Password)
	if err != nil {
		utils.Error(w, http.StatusBadRequest, err.Error())
		return
	}
	account.Hash = hash
	// Save to db
	id, err := db.Manager.DS.SaveAccount(account)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadAccount implements endpoint POST /api/v1/account
func ReadAccount(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadAccount"

	idParam := request.PathParam("accountID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		// utils.Error() called by request.ParseParams
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Account
	accounts, err := db.Manager.DS.ReadAccount(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Account: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		// Set hash to ""
		for i := range accounts {
			accounts[i].Hash = ""
		}
		utils.LogInfof("[%s()] accounts length=%d", funcName, len(accounts))
		utils.JSON(w, http.StatusOK, accounts)
	}
}

// ReadAllAccounts implements endpoint POST /api/v1/account
func ReadAllAccounts(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllAccounts"
	// Get all accounts
	accounts, err := db.Manager.DS.ReadAllAccounts()
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Accounts: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		// Set hash to ""
		for i := range accounts {
			accounts[i].Hash = ""
		}
		utils.LogInfof("[%s()] accounts length=%d", funcName, len(accounts))
		utils.JSON(w, http.StatusOK, accounts)
	}
}

// UpdateAccount implements endpoint POST /api/v1/account
func UpdateAccount(w http.ResponseWriter, req *http.Request) {
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var account db.Account
	err = json.Unmarshal(body, &account)
	if err != nil {
		utils.LogErrorf("Error unmarshal account: %v", err)
		http.Error(w, "can't unmarshal account", http.StatusBadRequest)
		return
	}
	// generate hashed, salted password
	hash, err := utils.HashAndSalt(account.Password)
	if err != nil {
		utils.Error(w, http.StatusBadRequest, err.Error())
		return
	}
	account.Hash = hash
	// update account
	err = db.Manager.DS.UpdateAccount(account)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteAccount implements endpoint POST /api/v1/account
func DeleteAccount(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadAccount"

	idParam := request.PathParam("accountID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	err := db.Manager.DS.DeleteAccount(id)
	if err != nil {
		utils.LogError("Failed to delete account")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
