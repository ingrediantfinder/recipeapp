package constant

// CSVTimeFormat is the timestamp format found in input CSV files (ASD, Alloc Gas/Water, etc)
const CSVTimeFormat string = "2006/01/02 15:04:05"

// SecureCookieHash is the base string to generate the secure cookie hash
const SecureCookieHash = "STIRcQS3ZwxiEJR0VVZSBho5foIRBFSU"

// -------------------------
// Request header related constants

// ReqHeaderJWTToken is the header name for JWT token
const ReqHeaderJWTToken string = "jwt-token"

// JWTSigningMethod is the signing method for JWT
const JWTSigningMethod string = "HS256"

// JWTTokenSecret is the secret for JWT
const JWTTokenSecret = "LSg9D5HUdOk!WSkwY1J!%wb20kRKyUST"

// JWTSessionInMinutes is the number a minutes a JWT session lasts
const JWTSessionInMinutes = 60
