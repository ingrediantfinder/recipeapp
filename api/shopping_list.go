package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveShopping_list implements endpoint POST /api/v1/shopping_list
func SaveShopping_list(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var shopping_list db.Shopping_list
	shopping_list.User_id = ID
	err = json.Unmarshal(body, &shopping_list)
	if err != nil {
		utils.LogErrorf("Error unmarshal shopping_list: %v", err)
		http.Error(w, "can't unmarshal shopping_list", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveShopping_list(ID, shopping_list)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadShopping_list implements endpoint GET /api/v1/Shopping_list/{Shopping_listID}
func ReadShopping_list(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadShopping_list"

	idParam := request.PathParam("shopping_listID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadIngredient(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllShopping_lists implements endpoint GET /api/v1/shopping_list
func ReadAllShopping_lists(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllShopping_lists"
	// Get all shopping_lists
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllShopping_lists(ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Shopping_lists: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateShopping_list implements endpoint PUT /api/v1/shopping_list/{shopping_list}
func UpdateShopping_list(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateShopping_list"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var shopping_list db.Shopping_list
	err = json.Unmarshal(body, &shopping_list)
	if err != nil {
		utils.LogErrorf("Error unmarshal shopping_list: %v", err)
		http.Error(w, "can't unmarshal shopping_list", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("shopping_listID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	//var ID = tk.AccountID
	err = db.Manager.DS.UpdateShopping_list(shopping_list)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteShopping_list  implements endpoint DEL /api/v1/shopping_list/{shopping_list}
func DeleteShopping_list(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteShopping_list"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	idParam := request.PathParam("shopping_listID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err = db.Manager.DS.DeleteShopping_list(ID, id)
	if err != nil {
		utils.LogError("Failed to delete shopping_list")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
