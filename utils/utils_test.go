package utils

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

func TestFormatInClause(t *testing.T) {
	// non-empty readingTypes
	readingTypes := []string{"GR,LPERM", "MD (m)"}
	inClause := FormatInClause(readingTypes)
	if inClause != "'GR','LPERM','MD (m)'" {
		t.Fatalf("inClause %s != 'GR','LPERM','MD (m)'", inClause)
	}

	// empty readingTypes
	readingTypes = []string{}
	inClause = FormatInClause(readingTypes)
	if inClause != "" {
		t.Fatalf("inClause %s != ", inClause)
	}

	// multiple wellIDs
	wellIDs := []int32{1, 2, 3}
	inClause = strings.Trim(strings.Join(strings.Split(fmt.Sprint(wellIDs), " "), ","), "[]")
	if inClause != "1,2,3" {
		t.Fatalf("inClause %s != '1,2,3'", inClause)
	}
	// one wellID
	wellIDs = []int32{1}
	inClause = strings.Trim(strings.Join(strings.Split(fmt.Sprint(wellIDs), " "), ","), "[]")
	if inClause != "1" {
		t.Fatalf("inClause %s != '1'", inClause)
	}
	// empty wellIDs
	wellIDs = []int32{}
	inClause = strings.Trim(strings.Join(strings.Split(fmt.Sprint(wellIDs), " "), ","), "[]")
	if inClause != "" {
		t.Fatalf("inClause %s != ''", inClause)
	}
}

func TestFormatDate(t *testing.T) {
	date := time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC)
	formattedDate := FormatDate(date)
	if "2018-07-01" != formattedDate {
		t.Fatalf("date %s != 2018-07-01", formattedDate)
	}

	date = time.Time{}
	formattedDate = FormatDate(date)
	if "0001-01-01" != formattedDate {
		t.Fatalf("date %s != 0001-00-01", formattedDate)
	}
}

func TestFormatTimestamp(t *testing.T) {
	date := time.Date(2018, 7, 1, 16, 2, 30, 0, time.UTC)
	formattedTimestamp := FormatTimestamp(date)
	if "2018-07-01T16:02:30Z" != formattedTimestamp {
		t.Fatalf("date %s != 2018-07-01T16:02:30Z", formattedTimestamp)
	}

	date = time.Time{}
	formattedTimestamp = FormatTimestamp(date)
	if "0001-01-01T00:00:00Z" != formattedTimestamp {
		t.Fatalf("date %s != 0001-00-01T00:00:00Z", formattedTimestamp)
	}
}

func TestTimestampToDate(t *testing.T) {
	timestamp := time.Date(2018, 7, 1, 16, 2, 30, 0, time.UTC)
	expDate := time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC)
	date := TimestampToDate(timestamp)
	if date.Equal(expDate) == false {
		t.Fatalf("date %s != expDate %s", FormatTimestamp(date), FormatTimestamp(expDate))
	}
}

func TestTimeOps(t *testing.T) {
	now := time.Now()
	earlier := now.Add(-5 * time.Second)
	fmt.Println(FormatTimestamp(now), FormatTimestamp(earlier), now.After(earlier))
}

func TestFloat64POrDefault(t *testing.T) {
	fp := ToFloat64P(0.0)
	default1 := 1.0
	default2 := 2.0
	// fp should be returned
	if *Float64POrDefault(fp, default1, default2) != 0.0 {
		t.Fatalf("*fp != 0.0")
	}
	// default1 should be returned
	if *Float64POrDefault(nil, default1, default2) != 1.0 {
		t.Fatalf("*fp != 1.0")
	}
	// default2 should be returned
	if *Float64POrDefault(nil, math.NaN(), default2) != 2.0 {
		t.Fatalf("*fp != 2.0")
	}
}

func TestFloat64PToString(t *testing.T) {
	// First convert *float64 to string and then back
	f := 1.1
	s := Float64PToString(&f)
	if "1.100000" != s {
		t.Fatalf("1.100000 != %s", s)
	}
	fp, err := StringToFloat64P(s)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if math.Abs(*fp-f) > 0.001 {
		t.Fatalf("fp %f != f %f", *fp, f)
	}

	if "null" != Float64PToString(nil) {
		t.Fatalf("null != %s", Float64PToString(nil))
	}
	fp, err = StringToFloat64P("null")
	if err != nil {
		t.Fatalf(err.Error())
	}
	if fp != nil {
		t.Fatalf("fp %f != nil", *fp)
	}
}

func TestSJSON(t *testing.T) {
	// create a subunit category json
	subunitCategory := `{}`
	subunitCategory, _ = sjson.Set(subunitCategory, "category", "category")
	subunitCategory, _ = sjson.Set(subunitCategory, "subunit", "subunit")

	// A new jsonStr and embed subunitCategory in it
	jsonStr := `{}`
	jsonStr, _ = sjson.Set(jsonStr, "attr", "attr1")
	jsonStr, _ = sjson.SetRaw(jsonStr, "subunit", subunitCategory)
	jsonStr, _ = sjson.Set(jsonStr, "test", map[string]interface{}{"hello": "world"})
	// fmt.Println(jsonStr)

	// validate
	if "category" != gjson.Get(jsonStr, "subunit.category").String() {
		t.Fatalf("The json value for 'subunit.category' %s != 'category' ",
			gjson.Get(jsonStr, "subunit.category").String())
	}

	if "world" != gjson.Get(jsonStr, "test.hello").String() {
		t.Fatalf("The json value for 'subunit.category' %s != 'category' ",
			gjson.Get(jsonStr, "subunit.category").String())
	}

	// Test array
	arrayJSONStr := `[]`
	arrayJSONStr, _ = sjson.SetRaw(arrayJSONStr, "-1", subunitCategory)
	fmt.Println(arrayJSONStr)
	if gjson.Get(arrayJSONStr, "0.subunit").String() != "subunit" {
		t.Fatalf("The json value for '0.subunit' %s != 'subunit'",
			gjson.Get(arrayJSONStr, "0.subunit").String())
	}
}

func TestMinMaxFloat64(t *testing.T) {
	min, max := MinMaxFloat64([]float64{22.0, 17.0, -120.0, 1.0})
	if min != -120.0 {
		t.Fatalf("min != -120.0")
	}
	if max != 22.0 {
		t.Fatalf("max != 22.0")
	}
}

type Person struct {
	name   string
	age    int
	scores []*float64
}
type Community struct {
	name   string
	people map[string]*Person
}

func TestMapStruct(t *testing.T) {
	community := Community{}
	community.name = "Test Community"
	community.people = make(map[string]*Person, 5)
	persons := []string{"Alpha", "Bravo", "Charlie", "Delta", "Echo"}
	for i, p := range persons {
		community.people[p] = &Person{
			name:   p,
			age:    i,
			scores: make([]*float64, 0),
		}
	}
	// append score to Alpha
	fp, _ := StringToFloat64P("1.0")
	(*community.people["Alpha"]).scores = append((*community.people["Alpha"]).scores, fp)
	fp, _ = StringToFloat64P("2.0")
	(*community.people["Bravo"]).scores = append((*community.people["Bravo"]).scores, fp)
	fmt.Println("test")
}

func TestMarshalFloat(t *testing.T) {
	fSlice := []float64{0, 1, math.NaN(), 3}
	response, err := json.Marshal(fSlice)
	if err == nil {
		t.Errorf("Marshal of NaN should throw an error")
	}

	fpSlice := []*float64{ToFloat64P(0), ToFloat64P(1), ToFloat64P(math.NaN()), ToFloat64P(3)}
	response, err = json.Marshal(fpSlice)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(response))
}

func TestMkdirAll(t *testing.T) {
	path := "../testfiles/todelete"
	// test 0777
	err := MkdirAll(path, 0777)
	if err != nil {
		t.Error(err)
	}
	infoDir, err := os.Stat(path)
	if err != nil {
		t.Error(err)
	}
	mode := fmt.Sprintf("%s", infoDir.Mode())
	if mode != "drwxrwxrwx" {
		t.Errorf("os.Chmod() %s failed", mode)
	}
	err = os.Remove(path)
	if err != nil {
		t.Error(err)
	}
	// test 0644
	err = MkdirAll(path, 0644)
	if err != nil {
		t.Error(err)
	}
	infoDir, err = os.Stat(path)
	if err != nil {
		t.Error(err)
	}
	mode = fmt.Sprintf("%s", infoDir.Mode())
	if mode != "drw-r--r--" {
		t.Errorf("os.Chmod() %s failed", mode)
	}
	err = os.Remove(path)
	if err != nil {
		t.Error(err)
	}
}

func TestCORSString(t *testing.T) {
	var localhostRegexp = regexp.MustCompile(`http(s)*\:\/\/localhost(\:\d+)*$`)
	var bujoGroupRegexp = regexp.MustCompile(`http(s)*\:\/\/(.)*bujogroup.com$`)

	cases := []string{
		"http://localhost:3000",
		"https://client.bujo.bujogroup.com",
	}

	for _, origin := range cases {
		if localhostRegexp.MatchString(origin) || bujoGroupRegexp.MatchString(origin) {
		} else {
			t.Errorf("origin=%s does not match CORS strings", origin)
		}
	}
}
