Vendoring is made simple by govendor (https://github.com/kardianos/govendor). Below are commands to initialize, add and view external libraries.  
  
```bash  
# Install govendor  
go get -u github.com/kardianos/govendor  
  
# Initialization  
govendor init  
  
# Add existing GOPATH files to vendor.  
govendor add +external  
  
# View your work.  
govendor list  
  
# Update a specific vendor lib and its dependencies  
govendor fetch -tree github.com/mattn/go-sqlite3  
``` 

# How to run bujo

## Local env
```bash
# install homebrew (if not already installed)
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# install golang
brew install go

# ensure you have a .bash_profile script
if[[ ! -e ~/.bash_profile ]]; then echo "[ -r ~/.bashrc ] && . ~/.bashrc" > ~/.bash_profile; chmod +x ~/.bash_profile; fi

# ensure you have a .bashrc script
if [[ ! -e ~/.bashrc ]]; then touch ~/.bashrc; chmod +x ~/.bashrc; fi

#setup env variables for go development (assuming ~/src directly as described in General section)
echo "export GOROOT=\"/usr/local/go\"" >> ~/.bashrc
echo "export GOROOT=\"$HOME/"" >> ~/.bashrc
echo 'export GOROOT=$PATH:$GOPATH/bin' >> ~/.bashrc
```
## Docker Env
Convenienve scripts for bujo are located in the /scripts directory. At any time each buho component can be run in dev mode by executing its script. All but the postgres database run in interactive mode, which means that the component process will consume the terminal session that launches it and remains running until either the terminal session is closed or CTRL+C is invoked to stop the process.
```bash
# Run each of these commands in a new terminal window or tab

# Launch DB
~/recipeapp/scripts/runPostgresDBDev.sh

# Launch bujo server process. Add a '--skipAuth' option ot ignore authentication requirements
~/recipeapp/scripts/runCleanBujoDev.sh

# or run as a GO process
go clean; go build; go test -count=1 -p 1 ./...; DEPLOY_ENV=local ./bujo --skipAuth --enableCORS

```

## CORS Env
To ensure that CORS works properly on the localhost, run:
```bash
DEPLOY_ENV=local ./bujo --enableCORS
```
this has to only be done once to set up the deployment environment

To enable CORS when running BUJO run:
```bash
./bujo --enableCORS
```


# VSCode Debug Configuration

{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
       {
            "name": "GO: bujo",
            "type": "go",
            "request": "launch",
            "mode": "debug",
            "remotePath": "",
            "port": 2345,
            "host": "127.0.0.1",
            "program": "/Users/steve/gowork/src/bujo/server.go",
            "env": {
                "DEPLOY_ENV": "local"
            },
            //"args": ["--port=3000"],
            //"args": ["--port=3000", "--skipAuth"],
            "args": ["--port=3000", "--skipAuth", "--enableCORS"],
            "showLog": true
        },
        {
            "name": "GO: bujo command",
            "type": "go",
            "request": "launch",
            "mode": "debug",
            "remotePath": "",
            "port": 2345,
            "host": "127.0.0.1",
            "cwd": "/Users/steve/gowork/src/bujo",
            "program": "/Users/steve/gowork/src/bujo/cmd/csvmuncher/main.go",
            //"program": "/Users/steve/gowork/src/bujo/cmd/resetbeanup/main.go",
            "env": {
                "DEPLOY_ENV": "local"
            },
            //"args": ["--port=3000"],
            "args": ["--port=3000", "--skipAuth"],
            //"args": ["--port=3000", "--skipAuth", "--enableCORS"],
            "showLog": true
        },
    ]
}

# Markdown Syntax
Click for syntax guide: [link](https://agea.github.io/tutorial.md/)


# React

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

