# Bujo Docker  
  
## Docker installation on Mac  
  
```bash  
brew install docker  
```  
  
## Build and Deploy  
   
### Go Docker Container 
  
```bash  
# Build Sandman Docker Container  
./buildDocker local  
# Start Sandman Docker Container  
./deployDocker local  
# modify deployDocker if cmdline arguments are to be passed into the container  
```

### Go Docker Container Deployed to EKS and Postgres RDS  
  
```bash  
# --------------------------------------
# Prep work.  The following commands were tested on Steve's Mac
cd ~/.kube
./kubeConfig bujo-fargate
kubectl -n prod get pods
# to get pods in all namespaces
kubectl get pods --all-namespaces
# login to prod and make sure it is functioning correctly
# sign in to AWS ECR
# connect to prod's RDS in pgadmin

# find bujo docker images
docker images |grep -w "bujo"

# --------------------------------------
# be sure to build the release branch
go clean; go build
gotest

# Build Sandman Docker Container  
./buildDocker prod release-20200131-b
# test built docker on local mac first
./deployDocker local release-20200131-b
# push to ECR or Artifactory  
./pushDocker ecr release-20200131-b
# liquibase update
liquibase/liquibase --username=bujostage --password=${DB_PASSWORD} --defaultsFile liquibase/liquibase-stage.properties update
liquibase/liquibase --username=bujoprod --password=${DB_PASSWORD} --defaultsFile liquibase/liquibase-prod.properties update
# execution migration scripts 
./scripts/migration/run.sh
# rolling update pods only
./rollingUpdatePods prod release-20200131-b

# --------------------------------------
# mv ./scripts/migration/* ./scripts/migration/archived/.
# update master.changelog.xml
# merge the release branch to master and develop

# --------------------------------------
# delete deployment
kubectl delete -n prod deployment bujo-prod
# create deployment
kubectl create -f aws/k8s/bujo-deploy-stage.yaml

# deploy to eks: depoyment, service and ingress
./deployDocker prod  
# delete bujo-prod  
./deployDocker prod delete  

# --------------------------------------
# watch the log of a pod  
kubectl -n prod logs -f <pod-id>  
# login to a pod
kubectl -n prod exec -it bujo-stage-5ff8448fdc-bnh9j /bin/bash

# --------------------------------------
# find all the images in a cluster  
kubectl get pods --all-namespaces -o jsonpath="{..image}" | tr -s '[[:space:]]' '\n' | sort | uniq -c
```  

## Push Docker Image  
  
```bash  
# push to ECR   
./pushDocker ecr bujo:release-20200314  
  
## Rolling Update Kubernetes Pods  
  
```bash  
# rolling update  
./rollingUpdatePods prod release-20200314
```  
