package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveRecipe_image implements endpoint POST /api/v1/Recipe_image
func SaveRecipe_image(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe_image db.Recipe_image
	err = json.Unmarshal(body, &recipe_image)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe_image: %v", err)
		http.Error(w, "can't unmarshal recipe_image", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveRecipe_image(ID, recipe_image)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadRecipe_image implements endpoint GET /api/v1/recipe_image/{recipe_image}
func ReadRecipe_image(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadRecipe_image"

	idParam := request.PathParam("recipe_imageID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Recipe_image
	results, err := db.Manager.DS.ReadRecipe_image(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipe_image: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllRecipe_images implements endpoint GET /api/v1/recipe_image
func ReadAllRecipe_images(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllRecipe_images"
	// Get all recipe_images
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllRecipe_images(ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipe_images: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateRecipe_image implements endpoint PUT /api/v1/recipe_image/{recipe_image}
func UpdateRecipe_image(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateRecipe_image"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe_image db.Recipe_image
	err = json.Unmarshal(body, &recipe_image)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe_image: %v", err)
		http.Error(w, "can't unmarshal recipe_image", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("recipe_imageID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()
	recipe_image.ID = idParam.AsInt32()
	err = db.Manager.DS.UpdateRecipe_image(recipe_image)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteRecipe_image  implements endpoint DEL /api/v1/recipe_image/{recipe_image}
func DeleteRecipe_image(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteRecipe_image"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	idParam := request.PathParam("recipe_imageID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()
	err = db.Manager.DS.DeleteRecipe_image(ID, id)
	if err != nil {
		utils.LogError("Failed to delete recipe_image")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
