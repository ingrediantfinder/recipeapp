package utils

import (
	"testing"
)

func TestLogger(t *testing.T) {

	Initialize("..")
	LogInfof("%s %s", "hello", "world")

	LogInfo("OMG Some info")
	LogInfof("OMG Some info int=%d, str=%s", 10, "name")
	LogError("OMG An Error!")

	LogInfo("OMG\ninfo\nwith\nnewlines")
	LogDebug("this shouldn't appear")
}
