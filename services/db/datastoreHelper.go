package db

import (
	"RecipeApp/utils"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"math"
	"time"
)

// writeFloat64 writes out "null" for a NaN float64.  Otherwise it writes out f in .15G precision
// Useful in saving value to db
func writeFloat64(f float64) string {
	if math.IsNaN(f) {
		return "null"
	}
	return fmt.Sprintf("%.15G", f)
}

// fromNullFloat64 returns math.Nan() for an empty db value.  Otherwise, it returns the float64 value
// Useful in reading value from db
func fromNullFloat64(nf sql.NullFloat64) float64 {
	if !nf.Valid {
		return math.NaN()
	}
	return nf.Float64
}

// writeFloat64P writes out "null" for a nil *float64.  Otherwise it writes out fp in .15G precison
// Useful in saving value to db
func writeFloat64P(fp *float64) string {
	if fp == nil {
		return "null"
	}
	return fmt.Sprintf("%.15G", *fp)
}

// fromNullFloat64P returns nil for an empty db value.  Otherwise it returns the *float64 value
// Useful in reading value from db
func fromNullFloat64P(nf sql.NullFloat64) *float64 {
	if !nf.Valid {
		return nil
	}
	return &(nf.Float64)
}

// writeInt32 writes out "null" for a nil *int32.  Otherwise it writes out ip
// Useful in saving value to db
func writeInt32P(ip *int32) string {
	if ip == nil {
		return "null"
	}
	return fmt.Sprintf("%d", *ip)
}

// fromNullInt32P returns nil for an empty db value.  Otherwise it returns the *int32 value
// Useful in reading value from db
func fromNullInt32P(ni sql.NullInt64) *int32 {
	if !ni.Valid {
		return nil
	}
	i := int32(ni.Int64)
	return &i
}

// fromNullInt64P returns nil for an empty db value.  Otherwise it returns the *int64 value
// Useful in reading value from db
func fromNullInt64P(ni sql.NullInt64) *int64 {
	if !ni.Valid {
		return nil
	}
	i := ni.Int64
	return &i
}

// fromNullBoolFalse returns False for an empty db value.  Otherwise, it returns the bool value
// Useful in reading value from db
func fromNullBoolFalse(nb sql.NullBool) bool {
	if !nb.Valid {
		return false
	}
	return nb.Bool
}

// fromNullBoolTrue returns True for an empty db value.  Otherwise, it returns the bool value
// Useful in reading value from db
func fromNullBoolTrue(nb sql.NullBool) bool {
	if !nb.Valid {
		return true
	}
	return nb.Bool
}

// writeTime returns null for a Zero time.Time value. Otherwise, it returns a formatted time string
// Useful in saving value to db
func writeTime(t time.Time) string {
	if t.IsZero() {
		return "null"
	}
	return fmt.Sprintf("'%s'", utils.FormatDate(t))
}

// writeTimestampP returns null for a nil time.Time value. Otherwise, it returns a formatted timestamp string.
// Useful in saving value to db
func writeTimestampP(t *time.Time) string {
	if t == nil || (*t).IsZero() {
		return "null"
	}
	return fmt.Sprintf("'%s'", utils.FormatTimestamp(*t))
}

// fromNullTime returns Zero time.Time value for an empty db value.  Otherwise, it returns a time.Time value
// Useful in reading value from db
func fromNullTime(nt NullTime) time.Time {
	if !nt.Valid {
		return time.Time{}
	}
	return nt.Time
}

// fromNullTimeP returns nil for an empty db value.  Otherwise, it returns a time.Time value
// Useful in reading value from db
func fromNullTimeP(nt NullTime) *time.Time {
	if !nt.Valid {
		return nil
	}
	return &(nt.Time)
}

// writeString writes out "null" for a nil string.  Otherwise it writes out s
// Useful in saving value to db
func writeStringP(s *string) string {
	if s == nil {
		return "null"
	}
	return fmt.Sprintf("'%s'", *s)
}

// fromNullStringP returns nil for an empty db value.  Otherwise it returns the *string value
// Useful in reading value from db
func fromNullStringP(ns sql.NullString) *string {
	if !ns.Valid {
		return nil
	}
	return &(ns.String)
}

// fromNullString returns "" for an empty db value.  Otherwise it returns the string value
// Useful in reading value from db
func fromNullString(ns sql.NullString) string {
	if !ns.Valid {
		return ""
	}
	return ns.String
}

// writeComp returns the proper comparison for a value, using "is" for null or "=" for a non-null value.
// Useful in WHERE clauses.
func writeComp(s string) string {
	preface := ""
	if s == "null" {
		preface = " is "
	} else {
		preface = "="
	}
	return preface + s
}

// ---------------------------
// Support Scanning of null value for time.Time

// NullTime supports scanning of null db value for time.Time
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}
