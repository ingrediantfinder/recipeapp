module RecipeApp

go 1.17

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.10.2
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/tidwall/gjson v1.8.1
	github.com/tidwall/match v1.0.3
	github.com/tidwall/sjson v1.1.7
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
)

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/context v1.1.1 // indirect
	github.com/tidwall/pretty v1.1.0 // indirect
)
