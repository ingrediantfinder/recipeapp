package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveRecipeLabel implements endpoint POST /api/v1/recipe_label
func SaveRecipeLabel(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe_label db.RecipeLabel
	err = json.Unmarshal(body, &recipe_label)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe_label: %v", err)
		http.Error(w, "can't unmarshal recipe_label", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveRecipeLabel(ID, recipe_label)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadRecipeLabel implements endpoint GET /api/v1/recipe_label/{recipe_label}
func ReadRecipeLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadRecipeLabel"

	idParam := request.PathParam("recipe_labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get RecipeLabel
	results, err := db.Manager.DS.ReadRecipeLabel(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading RecipeLabel: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllRecipeLabels implements endpoint GET /api/v1/recipe_label
func ReadAllRecipeLabels(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllRecipeLabels"
	// Get all recipe_labels
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID int32
	ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllRecipeLabels(ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading RecipeLabels: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateRecipeLabel implements endpoint PUT /api/v1/recipe_label/{recipe_label}
func UpdateRecipeLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateRecipeLabel"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe_label db.RecipeLabel
	err = json.Unmarshal(body, &recipe_label)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe_label: %v", err)
		http.Error(w, "can't unmarshal recipe_label", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("recipe_labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	//var ID = tk.AccountID
	recipe_label.ID = idParam.AsInt32()
	err = db.Manager.DS.UpdateRecipeLabel(recipe_label)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteRecipeLabel  implements endpoint DEL /api/v1/recipe_label/{recipe_label}
func DeleteRecipeLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteRecipeLabel"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	idParam := request.PathParam("recipe_labelID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err = db.Manager.DS.DeleteRecipeLabel(ID, id)
	if err != nil {
		utils.LogError("Failed to delete recipe_label")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
