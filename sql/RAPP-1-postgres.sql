-- liquibase formatted sql

-- changeset skuo:Recipe-1.0
-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "account" (
    id serial NOT NULL,
    firstname character varying(80) NOT NULL,
    lastname character varying(80) NOT NULL,
    email character varying(80) NOT NULL,
    lowercase_email character varying(80) NOT NULL,
    idx_account_lowercase_email character varying(80),
    hash character varying(255) NOT NULL DEFAULT '',
    updated_at TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT account_pkey PRIMARY KEY (id),
    CONSTRAINT account_lowercase_name_uniq UNIQUE(lowercase_email)
)
WITH (OIDS=FALSE); 
-- rollback drop table if exists "account";
-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "ingredient" (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    recipe_id INTEGER NOT NULL ,
    adjective character varying(255),
    quantity INTEGER NOT NULL ,
    unit character varying(255),
    updated_at TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT ingredient_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE); 
-- rollback drop table if exists "ingredient";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "recipe" (
    id serial NOT NULL,
    account_id INTEGER NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    instruction text,
    cooktime text,
    label_id INTEGER,
    updated_at TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT recipe_pkey PRIMARY KEY (id)
) 

WITH (OIDS=FALSE); 
-- rollback drop table if exists "recipe";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "label" (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT label_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "label";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "recipe_label" (
    id serial NOT NULL,
    recipe_id INTEGER NOT NULL,
    label_id INTEGER NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT recipe_label_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "recipe_label";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "day" (
    id serial NOT NULL,
    week INTEGER NOT NULL,`
    daynum INTEGER NOT NULL,
    week_day character varying(255) NOT NULL UNIQUE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT day_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "day";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "day_recipe" (
    id serial NOT NULL,
    day_id INTEGER NOT NULL,
    recipe_id INTEGER NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT day_recipe_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "day_recipe";