package main

import (
	"RecipeApp/api"
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

// Regex exp matches only localhost origins
var localhostRegexp = regexp.MustCompile(`http(s)*\:\/\/localhost(\:\d+)*$`)
var bujoGroupRegexp = regexp.MustCompile(`http(s)*\:\/\/(.)*bujogroup.com$`)

var skipAuthPtr *bool
var enableCORSPtr *bool

func main() {
	portPtr := flag.String("port", "3000", "default port for bujo.")
	skipAuthPtr = flag.Bool("skipAuth", false, "skip authentication")
	enableCORSPtr = flag.Bool("enableCORS", true, "enable CORS")
	flag.Parse()

	router := mux.NewRouter()
	AddMiddleWares(router)
	ServeAPI(router)
	serveFrontEndConfig(router)

	// initialize config
	utils.Initialize(".")

	// initialize db
	initDB()

	// utils.LogInfof("hello, world Bujo")
	// utils.LogInfof(reverse("hello, world Bujo"))

	utils.LogInfof("Start recipe_app server at port=%s skipAuth=%t enableCORS=%t\n", *portPtr, *skipAuthPtr, *enableCORSPtr)

	StartServer(router, portPtr)

}

// ServeAPI defines api endpoints
func ServeAPI(router *mux.Router) {
	//router.HandleFunc("/api/hello", APIHello)
	//router.HandleFunc("/api/error", APIError).Methods("GET", "POST", "OPTIONS")
	//router.PathPrefix("/api/ajaxlogin").Methods("GET", "POST", "OPTIONS").HandlerFunc(APILogin)
	//router.PathPrefix("/api/corslogin").Methods("GET", "POST", "OPTIONS").HandlerFunc(APICORSLogin)
	//router.PathPrefix("/api/readcookies").Methods("GET", "POST", "OPTIONS").HandlerFunc(APIReadCookies)

	// authentication
	router.HandleFunc("/api/v1/login", api.Login).Methods("POST", "OPTIONS")

	//Account
	router.HandleFunc("/api/v1/account", api.SaveAccount).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/account/accountID/{accountID}", api.ReadAccount).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/account", api.ReadAllAccounts).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/account", api.UpdateAccount).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/account/accountID/{accountID}", api.DeleteAccount).Methods("DELETE", "OPTIONS")

	//Ingredient
	router.HandleFunc("/api/v1/ingredient/recipeID/{recipeID}", api.SaveIngredient).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/{ingredientID}", api.ReadIngredient).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/leave-out/recipeID/{recipeID}/ingredientID/{ingredientID}", api.LeaveIngredientOut).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/list-ingredients/recipeID/{recipeID}", api.ReadAllIngredients).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/filterrecipe/ingredientID/{ingredientID}", api.ReadRecipeFromIngredient).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/recipeID/{recipeID}/ingredientID/{ingredientID}", api.UpdateIngredient).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/ingredient/recipeID/{recipeID}/ingredientID/{ingredientID}", api.DeleteIngredient).Methods("DELETE", "OPTIONS")

	//Recipe
	router.HandleFunc("/api/v1/recipe", api.SaveRecipe).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/recipe/{recipeID}", api.ReadRecipe).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe/{recipeID}/label", api.ReadAllRecipeLabel).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe/search/{recipeName}", api.SearchReadAllRecipes).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/label/{recipeID}/recipe", api.SearchRecipe).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe", api.ReadAllRecipes).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe/{recipeID}", api.UpdateRecipe).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/recipe/{recipeID}", api.DeleteRecipe).Methods("DELETE", "OPTIONS")

	//Label
	router.HandleFunc("/api/v1/label", api.SaveLabel).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/label/{labelID}", api.ReadLabel).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/label", api.ReadAllLabels).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/label/{labelID}", api.UpdateLabel).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/label/{labelID}", api.DeleteLabel).Methods("DELETE", "OPTIONS")

	//RecipeLabel
	router.HandleFunc("/api/v1/recipelabel", api.SaveRecipeLabel).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/recipelabel/{recipe_labelID}", api.ReadRecipeLabel).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipelabel", api.ReadAllRecipeLabels).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipelabel/{recipe_labelID}", api.UpdateRecipeLabel).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/recipelabel/{recipe_labelID}", api.DeleteRecipeLabel).Methods("DELETE", "OPTIONS")

	//Day
	router.HandleFunc("/api/v1/day", api.SaveDay).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/day/{dayID}", api.ReadDay).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/day", api.ReadAllDays).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/day/{dayID}/recipe", api.ReadDayRecipes).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/day/{dayID}", api.UpdateDay).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/day/{dayID}", api.DeleteDay).Methods("DELETE", "OPTIONS")

	//DayRecipe
	router.HandleFunc("/api/v1/day-recipe", api.SaveDayRecipe).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/day-recipe/{day-recipeID}", api.ReadDayRecipe).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/day-recipe", api.ReadAllDayRecipes).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/day-recipe/{day-recipeID}", api.UpdateDayRecipe).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/day-recipe/{day-recipeID}", api.DeleteDayRecipe).Methods("DELETE", "OPTIONS")

	//uploadimage
	router.HandleFunc("/api/v1/uploadfile/{recipe-id}", api.UploadFile).Methods("POST", "OPTIONS")

	//Recipe_image
	router.HandleFunc("/api/v1/recipe_image", api.SaveRecipe_image).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/recipe_image/{recipe_imageID}", api.ReadRecipe_image).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe_image", api.ReadAllRecipe_images).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/recipe_image/{recipe_imageID}", api.UpdateRecipe_image).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/recipe_image/{recipe_imageID}", api.DeleteRecipe_image).Methods("DELETE", "OPTIONS")

	//Shopping_list
	router.HandleFunc("/api/v1/shopping_list", api.SaveShopping_list).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list/{shopping_listID}", api.ReadShopping_list).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list", api.ReadAllShopping_lists).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list/{shopping_listID}", api.UpdateShopping_list).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list/{shopping_listID}", api.DeleteShopping_list).Methods("DELETE", "OPTIONS")

	//Shopping_list_ingredient
	router.HandleFunc("/api/v1/shopping_list_ingredient/{recipeID}", api.SaveShopping_list_ingredient).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_ingredient/{shopping_list_ingredientID}", api.ReadShopping_list_ingredient).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_ingredient_all/{shopping_list_ingredientID}", api.ReadAllShopping_list_ingredients).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_ingredient/{shopping_list_ingredientID}", api.UpdateShopping_list_ingredient).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_ingredient/{shopping_list_ingredientID}", api.DeleteShopping_list_ingredient).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_ingredient/Totals/count/{shopping_list_ingredientID}", api.ReadIngredientTotals).Methods("GET", "OPTIONS")

	//Shopping_list_recipe
	router.HandleFunc("/api/v1/shopping_list_recipe", api.SaveShopping_list_recipe).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_recipe/{shopping_list_recipeID}", api.ReadShopping_list_recipe).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_recipe_ingredient/{shopping_list_recipeID}", api.ReadShopping_list_recipe_ingredient).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_by_recipe/{shopping_list_recipeID}", api.ReadShopping_list_by_recipe).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_recipe", api.ReadAllShopping_list_recipes).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_recipe/{shopping_list_recipeID}", api.UpdateShopping_list_recipe).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/v1/shopping_list_recipe/{shopping_list_recipeID}", api.DeleteShopping_list_recipe).Methods("DELETE", "OPTIONS")

	router.PathPrefix("/").Handler(http.StripPrefix("/images",
		http.FileServer(http.Dir("images/"))))
}

// StartServer starts the go http server
func StartServer(router *mux.Router, portPtr *string) error {
	srv := &http.Server{
		Handler:      router,
		Addr:         ":" + *portPtr,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	err := srv.ListenAndServe() // this will run...
	if err != nil {
		if strings.ToLower(err.Error()) != "http: server closed" {
			fmt.Printf("HTTP server returned with error: '%s'", err.Error())
		}
	}
	return err
}

// AddMiddleWares add CORS and Auth middlewares
func AddMiddleWares(router *mux.Router) {
	// add middlewares
	if *enableCORSPtr == true {
		router.Use(corsMW)
	}
	router.Use(authMw)
}

// corsMW adds the CORS Access-Control response header
func corsMW(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		origin := r.Header.Get("Origin")
		if origin != "" {
			utils.LogInfof("[corsMw]: origin=%s", origin)
		}
		// If header origin matches the localhost with any port, then include CORS response headers
		if localhostRegexp.MatchString(origin) || bujoGroupRegexp.MatchString(origin) {
			appendCORSResponseHeaders(w, origin)
			utils.LogInfof("[corsMw]: origin=%s", origin)
			// if method is OPTIONS, then returns
			if r.Method == "OPTIONS" {
				return
			}
		}
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

// Append CORS Access-Control response headers
func appendCORSResponseHeaders(wtr http.ResponseWriter, origin string) {
	// Don't let on that we allow any local host in the response, just output
	// the exact origin that passes the test.
	wtr.Header().Set("Access-Control-Allow-Origin", origin)
	wtr.Header().Set("Access-Control-Allow-Credentials", "true")
	wtr.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	wtr.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Jwt-token")
	wtr.Header().Set("Access-Control-Expose-Headers", "Jwt-Token")
}

// authMw authorizes access
func authMw(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI != "/" {
			utils.LogInfof("[authMw]: Method=%s, RequestURI=%s", r.Method, r.RequestURI)
		}

		if strings.HasPrefix(r.RequestURI, "/api/v1/login") || *skipAuthPtr ||
			(strings.HasPrefix(r.RequestURI, "/api/v1/account") && r.Method == "POST") ||
			(r.Method == "GET") { //||
			//strings.HasPrefix(r.RequestURI, "/api/v1") ||
			//strings.HasPrefix(r.RequestURI, "/images")*/
			// If not /api/ request then skip authentication check
			if r.RequestURI != "/" {
				utils.LogInfof("[authMw]: %s", r.RequestURI)
			}
			next.ServeHTTP(w, r)
			return
		}

		// Get and validate JWT token
		jwtToken := r.Header.Get(constant.ReqHeaderJWTToken)
		jwtToken = strings.TrimSpace(jwtToken)

		if jwtToken == "" {
			//w.WriteHeader(xinithttp.StatusUnauthorized)
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Empty JWT token")
			return
		}
		tk := &model.Token{}
		_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(constant.JWTTokenSecret), nil
		})
		if err != nil {
			utils.LogErrorf("Invalid JWT token for %s", tk.Email)
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid token")
			return
		}
		ID := tk.AccountID
		fmt.Println(ID)
		expiresAt := time.Unix(tk.StandardClaims.ExpiresAt, 0)
		newExpiresAt := time.Now().Add(time.Minute * constant.JWTSessionInMinutes)
		tk.StandardClaims.ExpiresAt = newExpiresAt.Unix()
		token := jwt.NewWithClaims(jwt.GetSigningMethod(constant.JWTSigningMethod), tk)
		tokenString, err := token.SignedString([]byte(constant.JWTTokenSecret))
		if err != nil {
			utils.LogErrorf(err.Error())
			utils.Error(w, http.StatusBadRequest, err.Error())
			w.WriteHeader(http.StatusBadRequest)
			io.WriteString(w, "Bad Request")
			return
		}
		w.Header().Set(constant.ReqHeaderJWTToken, tokenString)
		utils.LogInfof("JWT validated for %s, was expiring at %s, now expiring at %s",
			tk.Email, utils.FormatTimestamp(expiresAt), utils.FormatTimestamp(newExpiresAt))

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})

}

// serveFrontEndConfig serves the frontend config files files in slingshot/config
func serveFrontEndConfig(router *mux.Router) {
	deployEnvironment := os.Getenv("DEPLOY_ENV")
	configFilename := fmt.Sprintf("environment.%s.json", deployEnvironment)

	router.HandleFunc("/config/environment.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "slingshot/configs/"+configFilename)
	})
}

func initDB() {
	_, err := db.MakeManager("postgres")
	utils.CheckErr(err)
}
