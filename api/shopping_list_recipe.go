package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// SaveShopping_list_recipe implements endpoint POST /api/v1/shopping_list_recipe
func SaveShopping_list_recipe(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var shopping_list_recipe db.Shopping_list_recipe
	err = json.Unmarshal(body, &shopping_list_recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal shopping_list_recipe: %v", err)
		http.Error(w, "can't unmarshal shopping_list_recipe", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveShopping_list_recipe(ID, shopping_list_recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadIngredient implements endpoint GET /api/v1/Shopping_list_recipe/{Shopping_list_recipeID}
func ReadShopping_list_recipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadShopping_list_recipe"

	idParam := request.PathParam("shopping_list_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadShopping_list_recipe(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadIngredient implements endpoint GET /api/v1/Shopping_list_recipe/{Shopping_list_recipeID}
func ReadShopping_list_recipe_ingredient(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadShopping_list_recipe"

	idParam := request.PathParam("shopping_list_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadShopping_list_recipe_ingredient(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadIngredient implements endpoint GET /api/v1/Shopping_list_recipe/{Shopping_list_recipeID}
func ReadShopping_list_by_recipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadShopping_list_recipe"

	idParam := request.PathParam("shopping_list_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Ingredient
	results, err := db.Manager.DS.ReadShopping_list_by_recipe(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Ingredient: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllShopping_list_recipes implements endpoint GET /api/v1/shopping_list_recipe
func ReadAllShopping_list_recipes(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllShopping_list_recipes"
	// Get all shopping_list_recipes
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllShopping_list_recipes(1)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Shopping_list_recipes: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateShopping_list_recipe implements endpoint PUT /api/v1/shopping_list_recipe/{shopping_list_recipe}
func UpdateShopping_list_recipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateShopping_list_recipe"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var shopping_list_recipe db.Shopping_list_recipe
	err = json.Unmarshal(body, &shopping_list_recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal shopping_list_recipe: %v", err)
		http.Error(w, "can't unmarshal shopping_list_recipe", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("shopping_list_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	//var ID = tk.AccountID
	err = db.Manager.DS.UpdateShopping_list_recipe(shopping_list_recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteShopping_list_recipe  implements endpoint DEL /api/v1/shopping_list_recipe/{shopping_list_recipe}
func DeleteShopping_list_recipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteShopping_list_recipe"
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})
	var ID = tk.AccountID
	idParam := request.PathParam("shopping_list_recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err = db.Manager.DS.DeleteShopping_list_recipe(ID, id)
	if err != nil {
		utils.LogError("Failed to delete shopping_list_recipe")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
