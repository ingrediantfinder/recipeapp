-- liquibase formatted sql

-- changeset tkuo:Recipe-2.0
-- Write idempotent DDL statements: CREATE, ALTER and DROP

CREATE TABLE IF NOT EXISTS "recipe_image" (
    id serial NOT NULL,
    recipe_id INTEGER NOT NULL,
    image_url character varying(255) NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT recipe_image_pkey PRIMARY KEY (id)
) 

WITH (OIDS=FALSE); 
-- rollback drop table if exists "recipe_image";