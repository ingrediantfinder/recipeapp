#!/bin/bash

## Creates/Starts Recipe Postgres database server on port 5432.
## Requires Docker.  Automatically pulls the postgres image if not available.
##
## Takes care of all docker container managment as well as all appropriate
## recipe db creation steps.  When this script is done, a Postgres docker
## image will be running in the background with liquibase-generated recipe
## schema.  If a docker container for Postgres is already running, this script
## will still execute a liquibase update to ensure that the current db schema
## is correct for this branch.

# Get the directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the name of this script
FILENAME="$(basename "$0")"
##### Docker container managment
CONTAINER_NAME=recipe_db
# Data volume name
VOLUME_NAME=recipe_db_volume
# Existing recipe postgres db volume
VOLUME_EXISTS=`docker volume ls -q -f name=$VOLUME_NAME`
# DB password
PASSWORD=my-secret-pw
# Port to expose db to host machine on
PORT=5433
# DB options
SM_ROLE="devuser"
SM_PW="devpassword"
SM_DB="recipedevdb"
# Util exec for psql
EXEC_PSQL="docker exec -it $CONTAINER_NAME psql -tA"
# liquibase properties that match our information
LIQUIBASE_PROPS="driver: org.postgresql.Driver
classpath: liquibase/postgresql-42.2.1.jar
changeLogFile: sql/master.changelog.xml
url: jdbc:postgresql://docker.for.mac.localhost:$PORT/$SM_DB
username: $SM_ROLE
password: $SM_PW"

# Name this terminal tab (works with iTerm as well)
echo -n -e "\033]0;$CONTAINER_NAME\007"

# Create volume for recipe db if it doesn't exist
if [[ $VOLUME_EXISTS == "" ]]; then
  echo "Creating db data volume: $VOLUME_NAME"
  docker volume create $VOLUME_NAME
fi
# Determine startup type.  After this block db image will be running in background if its not already
if [[ `docker ps -q -f name=^$CONTAINER_NAME$` == "" ]]; then
    # No running container with our name
    if [[ `docker ps -aq -f status=exited -f name=^$CONTAINER_NAME$` == "" ]]; then
        # No stopped container, create new
        echo "Creating db docker image"
        docker run -d --name $CONTAINER_NAME -e POSTGRES_PASSWORD=$PASSWORD -v "$DIR/..":/recipe --mount type=volume,src=$VOLUME_NAME,dst=/var/lib/postgresql/data -p $PORT:5432 postgres:10.4
    else
        # Found stopped container, start back up
        echo "Restarting db docker image"
        docker start $CONTAINER_NAME
    fi
    # Wait for our container to start before continuing
    until $EXEC_PSQL -U postgres -c "SELECT version()" &> /dev/null
    do
        echo "waiting for recipe db to start.."
        sleep 2
    done
else
    # Found running container already
    echo "Db docker image already running"
fi

# Determine if recipe db needs to be initialized
if [[ `$EXEC_PSQL -U postgres -c "SELECT rolname FROM pg_catalog.pg_roles WHERE rolname = '$SM_ROLE';"` == "" ]]; then
    echo "New volume detected, initializing recipe db"
    # The default recipe role doesn't exist.  So lets initialize everything
    # create role
    $EXEC_PSQL -U postgres -c "CREATE ROLE $SM_ROLE WITH LOGIN PASSWORD '$SM_PW' VALID UNTIL 'infinity';"
    # create database
    $EXEC_PSQL -U postgres -c "CREATE DATABASE $SM_DB WITH ENCODING='UTF8' OWNER=$SM_ROLE CONNECTION LIMIT=-1;"
    # init database props
    $EXEC_PSQL -U postgres -d $SM_DB -f "/recipe/sql/initDBProps.sql"
fi

# Run liquibase to build/ensure correct db schema
echo "Updating recipe db to latest schema"
# Dump liquibase props to a tmp file, then execute liquibase in java container
RUN_CMD="echo '$LIQUIBASE_PROPS' > /tmp/liquibase.properties;liquibase/liquibase --defaultsFile /tmp/liquibase.properties update"
docker run --rm -it -v "$DIR/..":/recipe -w /recipe openjdk:8-jre /bin/bash -c "$RUN_CMD"
# Make sure this term is in the project directory for convenience
cd "$DIR/.."
echo "Recipe db running."
echo "At any time run $FILENAME to ensure db schema matches your working copy."