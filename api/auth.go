package api

import (
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Login valid a user's password and set a JWT token
func Login(w http.ResponseWriter, req *http.Request) {
	funcName := "api.Login"

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("[%s()] Error reading body: %v", funcName, err)
		utils.LogError(errStr)
		utils.Error(w, http.StatusBadRequest, errStr)
		return
	}
	s := string(body)
	fmt.Println(s)

	var account db.Account
	err = json.Unmarshal(body, &account)
	if err != nil {
		utils.LogErrorf("[%s()] Error unmarshal account: %v", funcName, err)
		http.Error(w, "can't unmarshal account", http.StatusBadRequest)
		return
	}

	// find db account by email
	dbAccount, err := db.Manager.DS.ReadAccountByEmail(account.Email)
	if err != nil {
		errStr := fmt.Sprintf("[%s()] Error unmarshal account: %v", funcName, err)
		utils.LogErrorf(errStr)
		utils.Error(w, http.StatusBadRequest, errStr)
		return
	}
	if !utils.ComparePasswords(dbAccount.Hash, account.Password) {
		errStr := fmt.Sprintf("[%s()] Invalid password", funcName)
		utils.LogErrorf(errStr)
		utils.Error(w, http.StatusBadRequest, errStr)
		return
	}

	// generate and return JWT
	expiresAt := time.Now().Add(time.Minute * constant.JWTSessionInMinutes).Unix()
	tk := &model.Token{
		AccountID: dbAccount.ID,
		FirstName: dbAccount.FirstName,
		LastName:  dbAccount.LastName,
		Email:     dbAccount.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod(constant.JWTSigningMethod), tk)
	tokenString, err := token.SignedString([]byte(constant.JWTTokenSecret))
	if err != nil {
		utils.LogErrorf(err.Error())
		utils.Error(w, http.StatusBadRequest, err.Error())
		return
	}
	w.Header().Set(constant.ReqHeaderJWTToken, tokenString)
	utils.JSONStr(w, http.StatusOK, "")

	//get account
	results, err := db.Manager.DS.ReadAccount(dbAccount.ID)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Account: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}
