package utils

import (
	"fmt"
	"os"
	"testing"
)

func TestGJSONForServer(t *testing.T) {
	os.Unsetenv("DEPLOY_ENV")
	// DEPLOY_ENV is not set.  This is the same as calling
	// InitConfig([]string{"../recipefiles/recipe.json"})
	Initialize("..")

	result := GetConfig("websocket.enabled")
	if true != result.Bool() {
		t.Fatalf("[websocket.enabled]: true != %t", result.Bool())
	}

	result = GetConfig("websocket.missing")
	if false != result.Exists() {
		t.Fatalf("[websocket.missing] != false")
	}
}

func TestGJSONForTest(t *testing.T) {
	// DEPLOY_ENV is set to test.  This is the same as calling
	// InitConfig([]string{"../recipefiles/recipe-test.json", "../recipefiles/recipe.json"})
	os.Setenv("DEPLOY_ENV", "test")
	Initialize("..")

	result := GetConfig("websocket.enabled")
	if false != result.Bool() {
		t.Fatalf("[websocket.enabled]: false != %t", result.Bool())
	}

	result = GetConfig("websocket.missing")
	if false != result.Exists() {
		t.Fatalf("[websocket.missing] != false")
	}
}

func TestGetSecretFromEnv(t *testing.T) {
	os.Unsetenv("DEPLOY_ENV")
	// set up env variables
	os.Setenv("db.port", "5432")
	os.Setenv("db.user", "devuser")
	Initialize("..")

	// from envs
	result := GetSecret("db.port")
	if 5432 != result.Int() {
		t.Fatalf("[db.port] 5432 != %d", result.Int())
	}

	result = GetSecret("db.user")
	if "devuser" != result.String() {
		t.Fatalf("[db.user] devuser != %s", result.String())
	}

	// from ../recipefiles/recipe.json
	result = GetSecret("db.name")
	if "recipedevdb" != result.String() {
		t.Fatalf("[db.name] recipedevdb != %s", result.String())
	}

	result = GetSecret("db.password")
	if "devpassword" != result.String() {
		t.Fatalf("[db.password] devpassword != %s", result.String())
	}
}

func TestGetSecretFromFile(t *testing.T) {
	os.Setenv("DEPLOY_ENV", "test")
	os.Unsetenv("db.user")
	// set up env variables
	os.Setenv("db.port", "5432")
	Initialize("..")

	// from envs
	result := GetSecret("db.port")
	if 5432 != result.Int() {
		t.Fatalf("[db.port] 5432 != %d", result.Int())
	}

	// from ../smfiles/sm-test.json
	result = GetSecret("db.password")
	fmt.Println(result.String())
	if "testpassword" != result.String() {
		t.Fatalf("[db.password] testpassword != %s", result.String())
	}

	result = GetSecret("db.user")
	fmt.Println(result.String())
	if "testuser" != result.String() {
		t.Fatalf("[db.user] testuser != %s", result.String())
	}

	// from ../recipefiles/recipe.json
	result = GetSecret("db.name")
	if "recipedevdb" != result.String() {
		t.Fatalf("[db.name] recipedevdb != %s", result.String())
	}

}
