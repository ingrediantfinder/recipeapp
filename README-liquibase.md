# Bujo LiquiBase  
  
## LiquiBase  
  
LiquiBase (http://www.liquibase.org) is a Java data migration tool that updates, tags and rollback database changes. LiquiBase execution script and Java jars are stored in the liquibase/ directory. The database change SQL scripts are stored in the sql/ directory.  
  
### Install Java on Mac  
  
To be completed.  
  
### Update the Database  
  
```bash  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties update  
```  
  
### Tag the Database  
  
```bash  
# tag command tags the latest db change with a label  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties tag liquibase-test  
```  
  
### Rollback  
  
```bash  
# rollback rolls all new changes since the specified tag  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties rollback liquibase-test  
# rollbackCount rolls back the specified number of changes  
liquibase/liquibase  --defaultsFile liquibase/liquibase.properties rollbackCount 3  
```  
  
### Sample Execution Sequence  
  
```bash  
# update liquibase-test and tag  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties update  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties tag liquibase-test  
# add SM-3-postgres.sql to master.changelog.xml and then update  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties update  
# rollback by tag  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties rollback liquibase-test  
# rollback the 3 changes from liquibase-test  
liquibase/liquibase --defaultsFile liquibase/liquibase.properties rollbackCount 3  
```  
  
