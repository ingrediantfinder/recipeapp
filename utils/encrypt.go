package utils

import (
	"golang.org/x/crypto/bcrypt"
)

// HashAndSalt returns a password hash and salt used
func HashAndSalt(passwd string) (string, error) {
	bytes := []byte(passwd)
	hash, err := bcrypt.GenerateFromPassword(bytes, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), err
}

// ComparePasswords compares the hashed password with input password
func ComparePasswords(hashedPasswd, passwd string) bool {
	hashedPasswdBytes, passwdBytes := []byte(hashedPasswd), []byte(passwd)

	err := bcrypt.CompareHashAndPassword(hashedPasswdBytes, passwdBytes)
	if err != nil {
		LogError(err.Error())
		return false
	}
	return true
}
