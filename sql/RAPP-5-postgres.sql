-- liquibase formatted sql

-- changeset tkuo:Recipe-5

ALTER TABLE shopping_list_ingredient ADD shopping_list INTEGER NOT NULL

-- rollback ALTER TABLE shopping_list_recipe DROP shopping_list;