package main

import (
	"RecipeApp/utils"
	"fmt"
	"net/http"
)

// APIHello extract user info from jwt
func APIHello(w http.ResponseWriter, req *http.Request) {
	//utils.LogInfof("<APIHello>")
	message := "Hello there from the API world!!!"
	utils.LogInfof(message)

	w.Write([]byte(message))
}

// APIError returns an error message
func APIError(w http.ResponseWriter, req *http.Request) {
	//fmt.Println("<APIError>")
	message := "OMG - an error has occurred!!!"
	utils.LogInfof(message)

	//w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(message))
}

// APILogin mimicks a login by setting a cookie
func APILogin(w http.ResponseWriter, req *http.Request) {
	utils.LogInfof("<APILogin>")
	cookie := &http.Cookie{Name: "ajaxSession", Value: "ajaxCookie", Path: "/", HttpOnly: true,
		MaxAge: int(300)}
	http.SetCookie(w, cookie)
	w.Write([]byte("{ \"ajaxToken\": \"ajaxToken\"}"))
}

// APICORSLogin mimicks a login by setting a cookie
func APICORSLogin(w http.ResponseWriter, req *http.Request) {
	utils.LogInfof("<APICORSLogin>")
	cookie := &http.Cookie{Name: "corsSession", Value: "corsCookie", Path: "/", HttpOnly: true,
		MaxAge: int(300)}
	http.SetCookie(w, cookie)
	w.Write([]byte("{ \"corsToken\": \"corsToken\"}"))
}

// APIReadCookies mimicks a login by setting a cookie
func APIReadCookies(w http.ResponseWriter, req *http.Request) {
	utils.LogInfof("<APIReadCookies>")
	var message string
	message = message + fmt.Sprintf("Method: %s\n", req.Method)
	message = message + fmt.Sprintf("Header field Authorization, value %q\n", req.Header.Get("Authorization"))

	for _, cookieName := range []string{"ajaxSession", "corsSession", "blsessionid"} {
		utils.LogInfof("checking cookie name=%s\n", cookieName)
		cookie, _ := req.Cookie(cookieName)
		if cookie != nil {
			message = message + fmt.Sprintf("cookie %s=%s\n", cookie.Name, cookie.Value)
		}
	}

	utils.LogInfof(message)
	w.Write([]byte(message))
}
