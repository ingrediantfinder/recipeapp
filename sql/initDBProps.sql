-- Create devuser role and bujo database
-- Change 'devuser' to the desired one such as 'bujoprod'
CREATE ROLE devuser WITH LOGIN password 'devpassword' VALID UNTIL 'infinity';
CREATE DATABASE bujo WITH ENCODING='UTF8' OWNER=bujoprod CONNECTION LIMIT=-1;

-- Initialize Bujo db props
GRANT usage ON schema public TO devuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON all tables IN schema public TO devuser;
ALTER DEFAULT PRIVILEGES IN schema public GRANT SELECT, INSERT, UPDATE, DELETE ON tables TO devuser;
GRANT usage ON all sequences IN schema public to devuser;
ALTER DEFAULT PRIVILEGES IN schema public GRANT usage ON sequences TO devuser;
GRANT EXECUTE ON all functions IN schema public TO devuser;
ALTER DEFAULT PRIVILEGES IN schema public GRANT execute ON functions TO devuser;