package model

import "github.com/dgrijalva/jwt-go"

// Token defines a JWT token
type Token struct {
	AccountID int32  `json:"account_id"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Email     string `json:"email"`
	*jwt.StandardClaims
}
