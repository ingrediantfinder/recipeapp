package websocket

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options

// Progress establishes a WS connection and shows the progress of the input command
func Progress(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

Loop:
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		log.Printf("[websocket.Progress] recv: %s", message)
		err = c.WriteMessage(mt, message)
		if err != nil {
			log.Println("write:", err)
			break
		}

		// loop 10x, send a message after every second in an increment of 10%
		for i := 1; i <= 10; i++ {
			time.Sleep(500 * time.Millisecond)
			msgStr := fmt.Sprintf("%d%%", 10*i)
			err = writeMessage(c, mt, []byte(msgStr))
			if err != nil {
				log.Println("write:", err)
				break Loop
			}
		}
	}
}

func writeMessage(c *websocket.Conn, mt int, message []byte) error {
	err := c.WriteMessage(mt, message)
	return err
}
