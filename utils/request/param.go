package request

import (
	"fmt"
	"reflect"
)

// -------------------------------
// Param helper 'class'

// Param is a utility object to better organize parsing request
// parameters for REST endpoints, make code cleaner and eleminate copy/paste
// operations.  Tracks param name, if it's required, an optional default value
// (when not required) and a context (path/query) where the param is found.
// Other validation factors should be done by the rest endpoint (format/limits/etc)
type Param struct {
	Name         string
	Format       reflect.Kind
	Required     bool
	DefaultValue interface{}
	Context      string
	Value        interface{}
}

// PathParam creates and returns a new instance of a path Param.
// Path params are always required.
// name: Parameter name to match
// format: A reflect.Kind constant to identify the param type.
//   see ParsePrimitiveStringValue() for supported types.
// Note, param formats are used for parsing validation and value typing.  If you wish to be more
// flexible with the value, use reflect.String.
func PathParam(name string, format reflect.Kind) Param {
	return Param{
		Name:         name,
		Format:       format,
		Required:     true,
		DefaultValue: nil,
		Context:      "path",
	}
}

// QueryParam creates and returns a new instance of a query Param
// name: Parameter name to match
// format: A reflect.Kind constant to identify the param type.
//   see ParsePrimitiveStringValue() for supported types.
// required: true if parameter is required (endpoint should throw error if param does not exist)
// defaultValue: Only valid if required is 'false'.  Default value.  Set to nil for no
//   default, but that will cause errors on parsed types.
// Note, param formats are used for parsing validation and value typing.  If you wish to be more
// flexible with the value, use reflect.String.
func QueryParam(name string, format reflect.Kind, required bool, defaultValue interface{}) Param {
	return Param{
		Name:         name,
		Format:       format,
		Required:     required,
		DefaultValue: defaultValue,
		Context:      "query",
	}
}

// HeaderParam creates and returns a new instance of a header Param
// name: Parameter name to match
// format: A reflect.Kind constant to identify the param type.
//   see ParsePrimitiveStringValue() for supported types.
// required: true if parameter is required (endpoint should throw error if param does not exist)
// defaultValue: Only valid if required is 'false'.  Default value.  Set to nil for no
//   default, but that will cause errors on parsed types.
// Note, param formats are used for parsing validation and value typing.  If you wish to be more
// flexible with the value, use reflect.String.
func HeaderParam(name string, format reflect.Kind, required bool, defaultValue interface{}) Param {
	return Param{
		Name:         name,
		Format:       format,
		Required:     required,
		DefaultValue: defaultValue,
		Context:      "header",
	}
}

// IsEmpty returns true if this Request param has no value and no default.
func (r *Param) IsEmpty() bool {
	return r.Value == nil
}

// parseFromSet attempts to find a matching param for this
// Params criteria. should only be called by utility functions.
func (r *Param) parseFromSet(params map[string]string) error {
	return r.parseFromString(params[r.Name])
}

// parseFromMultiMap attempts to find the first query parameter matching this
// Params criteria. should only be called by utility functions.
func (r *Param) parseFromMultiMap(queryValues map[string][]string) error {
	valueArray := queryValues[r.Name]
	if len(valueArray) == 0 {
		return r.parseFromString("")
	}
	return r.parseFromString(valueArray[0])
}

// parseFromString attempts to format the given string as the value for this
// Param
func (r *Param) parseFromString(val string) error {
	if val == "" {
		if r.Required || r.DefaultValue == nil {
			r.Value = nil
		} else {
			r.Value = r.DefaultValue
		}
	} else {
		parsedVal, err := ParsePrimitiveStringValue(val, r.Format)
		if err != nil {
			r.Value = nil
			return err
		}
		r.Value = parsedVal
	}
	return nil
}

// AsString attempts to convert this Param's value to string
// returns zero value if conversion is not appropriate
func (r Param) AsString() string {
	if r.Value == nil {
		return ""
	}
	return fmt.Sprint(r.Value)
}

// AsBool attempts to convert this Param's value to bool
// returns false value if conversion is not appropriate
func (r Param) AsBool() bool {
	if val, ok := r.Value.(bool); ok {
		return bool(val)
	}
	return false
}

// AsFloat32 attempts to convert this Param's value to float32
// returns zero value if conversion is not appropriate
func (r Param) AsFloat32() float32 {
	if num, ok := r.Value.(float64); ok {
		return float32(num)
	}
	return 0
}

// AsFloat64 attempts to convert this Param's value to float64
// returns zero value if conversion is not appropriate
func (r Param) AsFloat64() float64 {
	if num, ok := r.Value.(float64); ok {
		return float64(num)
	}
	return 0
}

// AsInt8 attempts to convert this Param's value to int8
// returns zero value if conversion is not appropriate
func (r Param) AsInt8() int8 {
	if num, ok := r.Value.(int64); ok {
		return int8(num)
	}
	return 0
}

// AsInt16 attempts to convert this Param's value to int16
// returns zero value if conversion is not appropriate
func (r Param) AsInt16() int16 {
	if num, ok := r.Value.(int64); ok {
		return int16(num)
	}
	return 0
}

// AsInt32 attempts to convert this Param's value to int32
// returns zero value if conversion is not appropriate
func (r Param) AsInt32() int32 {
	if num, ok := r.Value.(int64); ok {
		return int32(num)
	}
	return 0
}

// AsInt64 attempts to convert this Param's value to int64
// returns zero value if conversion is not appropriate
func (r Param) AsInt64() int64 {
	if num, ok := r.Value.(int64); ok {
		return int64(num)
	}
	return 0
}

// AsUint8 attempts to convert this Param's value to uint8
// returns zero value if conversion is not appropriate
func (r Param) AsUint8() uint8 {
	if num, ok := r.Value.(uint64); ok {
		return uint8(num)
	}
	return 0
}

// AsUint16 attempts to convert this Param's value to uint16
// returns zero value if conversion is not appropriate
func (r Param) AsUint16() uint16 {
	if num, ok := r.Value.(uint64); ok {
		return uint16(num)
	}
	return 0
}

// AsUint32 attempts to convert this Param's value to uint32
// returns zero value if conversion is not appropriate
func (r Param) AsUint32() uint32 {
	if num, ok := r.Value.(uint64); ok {
		return uint32(num)
	}
	return 0
}

// AsUint64 attempts to convert this Param's value to uint64
// returns zero value if conversion is not appropriate
func (r Param) AsUint64() uint64 {
	if num, ok := r.Value.(uint64); ok {
		return uint64(num)
	}
	return 0
}
