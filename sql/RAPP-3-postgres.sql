-- liquibase formatted sql

-- changeset tkuo:Recipe-3.0
-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "shopping_list" (
    id serial NOT NULL,
    user_id INTEGER NOT NULL,
    name character varying(255) NOT NULL UNIQUE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT shopping_list_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "shopping_list";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "shopping_list_ingredient" (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    recipe_id INTEGER NOT NULL ,
    adjective character varying(255),
    quantity INTEGER NOT NULL ,
    unit character varying(255),
    updated_at TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT shopping_list_ingredient_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "shopping_list_ingredient";

-- Write idempotent DDL statements: CREATE, ALTER and DROP
CREATE TABLE IF NOT EXISTS "shopping_list_recipe" (
    id serial NOT NULL,
    shopping_list_id INTEGER NOT NULL,
    recipe_id INTEGER NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT shopping_list_recipe_pkey PRIMARY KEY (id)
) 
WITH (OIDS=FALSE); 
-- rollback drop table if exists "shopping_list_recipe";