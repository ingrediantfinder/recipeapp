package db

import (
	"errors"
	"strings"
	"time"
)

// DatastoreI defines the Bujo database functions
type DatastoreI interface {
	// Account
	SaveAccount(account Account) (int32, error)
	ReadAccount(ids ...int32) ([]Account, error)
	ReadAllAccounts() ([]Account, error)
	ReadAccountByEmail(email string) (Account, error)
	UpdateAccount(account Account) error
	DeleteAccount(id int32) error
	// Ingredient
	SaveIngredient(userid int32, Ingredient Ingredient) (int32, error)
	ReadIngredient(ids ...int32) ([]Ingredient, error)
	LeaveIngredientOut(rid int32, ids ...int32) ([]Ingredient, error)
	ReadAllIngredients(userid int32) ([]Ingredient, error)
	ReadRecipeFromIngredient(ingname string) ([]Recipe, error)
	UpdateIngredient(rid int32, Ingredient Ingredient) error
	DeleteIngredient(usrid int32, name string, rid int32) error
	// Recipe
	SaveRecipe(userid int32, recipe Recipe) (int32, error)
	ReadRecipe(ids ...int32) ([]Recipe, error)
	ReadAllRecipes(userid int32) ([]Recipe, error)
	SearchReadAllRecipes(userid int32, str string) ([]Recipe, error)
	ReadAllRecipeLabel(userid int32) ([]Label, error)
	SearchRecipe(ids ...int32) ([]Recipe, error)
	UpdateRecipe(id int32, recipe Recipe) error
	DeleteRecipe(usrid int32, id int32) error
	// Label
	SaveLabel(userid int32, label Label) (int32, error)
	ReadLabel(ids ...int32) ([]Label, error)
	ReadAllLabels(userid int32) ([]Label, error)
	UpdateLabel(label Label) error
	DeleteLabel(usrid int32, id int32) error
	// RecipeLabel
	SaveRecipeLabel(userid int32, recipe_label RecipeLabel) (int32, error)
	ReadRecipeLabel(ids ...int32) ([]RecipeLabel, error)
	ReadAllRecipeLabels(userid int32) ([]RecipeLabel, error)
	UpdateRecipeLabel(recipe_label RecipeLabel) error
	DeleteRecipeLabel(usrid int32, id int32) error
	// Day
	SaveDay(userid int32, day Day) (int32, error)
	ReadDay(ids ...int32) ([]Day, error)
	ReadAllDays(userid int32) ([]Day, error)
	ReadDayRecipes(userid int32) ([]Recipe, error)
	UpdateDay(day Day) error
	DeleteDay(usrid int32, id int32) error
	// DayRecipe
	SaveDayRecipe(userid int32, day_recipe DayRecipe) (int32, error)
	ReadDayRecipe(ids ...int32) ([]DayRecipe, error)
	ReadAllDayRecipes(userid int32) ([]DayRecipe, error)
	UpdateDayRecipe(day_recipe DayRecipe) error
	DeleteDayRecipe(usrid int32, id int32) error
	// Recipe_image
	SaveRecipe_image(userid int32, recipe_image Recipe_image) (int32, error)
	ReadRecipe_image(ids ...int32) ([]Recipe_image, error)
	ReadAllRecipe_images(userid int32) ([]Recipe_image, error)
	UpdateRecipe_image(recipe_image Recipe_image) error
	DeleteRecipe_image(usrid int32, id int32) error

	// Shopping_list
	SaveShopping_list(userid int32, shopping_list Shopping_list) (int32, error)
	ReadShopping_list(ids ...int32) ([]Shopping_list, error)
	ReadAllShopping_lists(userid int32) ([]Shopping_list, error)
	UpdateShopping_list(shopping_list Shopping_list) error
	DeleteShopping_list(usrid int32, id int32) error

	// Shopping_listRecipe
	SaveShopping_list_recipe(userid int32, shopping_list_recipe Shopping_list_recipe) (int32, error)
	ReadShopping_list_recipe(ids ...int32) ([]Recipe, error)
	ReadShopping_list_recipe_ingredient(ids ...int32) ([]Ingredient, error)
	ReadShopping_list_by_recipe(ids ...int32) ([]Shopping_list_recipe, error)
	ReadAllShopping_list_recipes(userid int32) ([]Shopping_list_recipe, error)
	UpdateShopping_list_recipe(shopping_list_recipe Shopping_list_recipe) error
	DeleteShopping_list_recipe(usrid int32, id int32) error

	// Shopping_list_ingredient
	SaveShopping_list_ingredient(userid int32, shopping_list_ingredient Shopping_list_ingredient) (int32, error)
	ReadShopping_list_ingredient(ids ...int32) ([]Shopping_list_ingredient, error)
	ReadAllShopping_list_ingredients(userid int32) ([]Shopping_list_ingredient, error)
	UpdateShopping_list_ingredient(shopping_list_ingredient Shopping_list_ingredient) error
	DeleteShopping_list_ingredient(usrid int32, id int32) error
	ReadIngredientTotals(userid int32) ([]Shopping_list_ingredient, error)
}

// --------------------------

// Manager manages the interactions with a datasource
var Manager *ManagerT

// ManagerT contains a DatabaseI
type ManagerT struct {
	DS DatastoreI
}

// MakeManager returns the appropriate ds based on input dbName
func MakeManager(dbName string) (*ManagerT, error) {
	var err error
	var database DatastoreI
	if dbName == "postgres" {
		database, err = initPostgres()
		Manager = &ManagerT{DS: database}
	} else {
		// not supported db
		err = errors.New(dbName + "is not supported")
	}
	return Manager, err
}

// =============================
// Data Object (DO) for database

// Account represents a account
type Account struct {
	ID             int32     `json:"id"`
	FirstName      string    `json:"firstname"`
	LastName       string    `json:"lastname"`
	Email          string    `json:"email"`
	LowercaseEmail string    `json:"lowercase_email"`
	UpdatedAt      time.Time `json:"updated_at"`
	Password       string    `json:"password,omitempty"` // used to send password, never returned
	Hash           string    `json:"hash,omitempty"`     // never sent back to FE
}

// SetEmail set a account's Email and turn email to lower case and use it to set LowercaseEmail
func (a *Account) SetEmail(email string) {
	a.Email = email
	a.LowercaseEmail = strings.ToLower(email)
}

// ---------------------------

// Ingredient represents a Ingredient
type Ingredient struct {
	ID          int32     `json:"id"`
	Name        string    `json:"name"`
	Quantity    int32     `json:"quantity,string"`
	RecipeID    int32     `json:"recipe_id"`
	Adjective   string    `json:"adjective"`
	Unit        string    `json:"unit"`
	UpdatedAt   time.Time `json:"updated_at"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------

// Recipe represents a recipe
type Recipe struct {
	ID          int32     `json:"id"`
	AccountID   int32     `json:"account_id"`
	BookID      int32     `json:"book_id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Instruction string    `json:"instruction"`
	Cooktime    string    `json:"cooktime"`
	Label       Label     `json:"label"`
	UpdatedAt   time.Time `json:"updated_at"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------

// label represents a label for label
type Label struct {
	ID          int32     `json:"id"`
	Name        string    `json:"name"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------
// recipelabel represents a label for recipelabel
type RecipeLabel struct {
	ID          int32     `json:"id"`
	Name        string    `json:"name"`
	RecipeID    int32     `json:"recipe_id"`
	LabelID     int32     `json:"label_id"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------
// day represents a label for day
type Day struct {
	ID          int32     `json:"id"`
	Week        int32     `json:"week"`
	Daynum      int32     `json:"daynum"`
	WeekDay     string    `json:"week_day"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------
// day_recipe represents a label for day
type DayRecipe struct {
	ID          int32     `json:"id"`
	DayID       int32     `json:"day_id"`
	RecipeID    int32     `json:"recipe_id"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------

// recipe_image represents a recipe_image for recipe_image
type Recipe_image struct {
	ID          int32     `json:"id"`
	Recipe_id   int32     `json:"recipe_id"`
	Image_url   string    `json:"image_url"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------
// shopping_list represents a label for shopping_list
type Shopping_list struct {
	ID          int32     `json:"id"`
	User_id     int32     `json:"user_id"`
	Name        string    `json:"name"`
	CreatededAt time.Time `json:"created_at"`
}

// ---------------------------
// shopping_list_recipe represents a label for shopping_list_recipe
type Shopping_list_recipe struct {
	ID              int32     `json:"id"`
	Shopping_listID int32     `json:"shopping_list_id"`
	RecipeID        int32     `json:"recipe_id"`
	CreatededAt     time.Time `json:"created_at"`
}

// ---------------------------
// Shopping_list_ingredient represents a Shopping_list_ingredient
type Shopping_list_ingredient struct {
	ID            int32     `json:"id"`
	Shopping_list int32     `json:"shopping_list"`
	Name          string    `json:"name"`
	Quantity      int32     `json:"quantity,string"`
	Adjective     string    `json:"adjective"`
	Unit          string    `json:"unit"`
	UpdatedAt     time.Time `json:"updated_at"`
	CreatededAt   time.Time `json:"created_at"`
}
