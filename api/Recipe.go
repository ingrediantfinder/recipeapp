package api

import (
	//"RecipeApp/constant"
	//"RecipeApp/model"
	"RecipeApp/constant"
	"RecipeApp/model"
	"RecipeApp/services/db"
	"RecipeApp/utils"
	"RecipeApp/utils/request"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"github.com/dgrijalva/jwt-go"
	//"strings"
	//"github.com/dgrijalva/jwt-go"
)

// SaveRecipe implements endpoint POST /api/v1/recipe
func SaveRecipe(w http.ResponseWriter, req *http.Request) {
	tk := &model.Token{}
	jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	jwtToken = strings.TrimSpace(jwtToken)

	_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(constant.JWTTokenSecret), nil
	})

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	fmt.Print(string(body))
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe db.Recipe
	err = json.Unmarshal(body, &recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe: %v", err)
		http.Error(w, "can't unmarshal recipe", http.StatusBadRequest)
		return
	}
	id, err := db.Manager.DS.SaveRecipe(1, recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, id)
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// ReadRecipe implements endpoint GET /api/v1/recipe/{recipe}
func ReadRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadRecipe"

	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Recipe
	results, err := db.Manager.DS.ReadRecipe(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipe: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllRecipes implements endpoint GET /api/v1/recipe
func ReadAllRecipes(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllRecipes"
	// Get all recipes
	//tk := &model.Token{}
	//jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	//jwtToken = strings.TrimSpace(jwtToken)

	//_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
	//	return []byte(constant.JWTTokenSecret), nil
	//})
	//var ID = tk.AccountID
	//utils.LogInfof("id=%d", ID)
	results, err := db.Manager.DS.ReadAllRecipes(1)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipes: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadAllRecipes implements endpoint GET /api/v1/recipe
func SearchReadAllRecipes(w http.ResponseWriter, req *http.Request) {
	funcName := "api.GetAllRecipes"

	idParam := request.PathParam("recipeName", reflect.String)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsString()
	id = strings.ToLower(id)
	id = strings.Title(id)

	results, err := db.Manager.DS.SearchReadAllRecipes(1, id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipes: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadRecipe implements endpoint GET /api/v1/recipe/{recipe}
func ReadAllRecipeLabel(w http.ResponseWriter, req *http.Request) {
	funcName := "api.ReadAllRecipeLabel"

	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Recipe
	results, err := db.Manager.DS.ReadAllRecipeLabel(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipe: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// ReadRecipe implements endpoint GET /api/v1/recipe/{recipe}
func SearchRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.SearchRecipe"

	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	// Get Recipe
	results, err := db.Manager.DS.SearchRecipe(id)
	if err != nil {
		utils.LogErrorf("[%s()] Error reading Recipe: %+v", funcName, err)
		utils.Error(w, http.StatusBadRequest, err.Error())
	} else {
		utils.LogInfof("[%s()] results length=%d", funcName, len(results))
		utils.JSON(w, http.StatusOK, results)
	}
}

// UpdateRecipe implements endpoint PUT /api/v1/recipe/{recipe}
func UpdateRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.UpdateRecipe"
	//tk := &model.Token{}
	//jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	//jwtToken = strings.TrimSpace(jwtToken)

	//_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
	//	return []byte(constant.JWTTokenSecret), nil
	//})
	//var ID = tk.AccountID
	//utils.LogInfof("id=%d", ID)

	// Read req's body and convert []byte to string
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	if err != nil {
		errStr := fmt.Sprintf("Error reading body: %v", err)
		utils.LogError(errStr)
		http.Error(w, errStr, http.StatusBadRequest)
		return
	}

	var recipe db.Recipe
	err = json.Unmarshal(body, &recipe)
	if err != nil {
		utils.LogErrorf("Error unmarshal recipe: %v", err)
		http.Error(w, "can't unmarshal recipe", http.StatusBadRequest)
		return
	}
	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	//id := idParam.AsInt32()

	recipe.ID = idParam.AsInt32()
	err = db.Manager.DS.UpdateRecipe(1, recipe)
	if err == nil {
		utils.JSON(w, http.StatusOK, "")
	} else {
		utils.Error(w, http.StatusBadRequest, err.Error())
	}
}

// DeleteRecipe  implements endpoint DEL /api/v1/recipe/{recipe}
func DeleteRecipe(w http.ResponseWriter, req *http.Request) {
	funcName := "api.DeleteRecipe"
	//tk := &model.Token{}
	//jwtToken := req.Header.Get(constant.ReqHeaderJWTToken)
	//jwtToken = strings.TrimSpace(jwtToken)

	//_, err := jwt.ParseWithClaims(jwtToken, tk, func(token *jwt.Token) (interface{}, error) {
	//	return []byte(constant.JWTTokenSecret), nil
	//})
	//var ID = tk.AccountID
	//utils.LogInfof("id=%d", ID)
	idParam := request.PathParam("recipeID", reflect.Int32)
	if !request.ParseParams(w, req, &idParam) {
		utils.LogErrorf("%s failed to parse path parameters", funcName)
		return
	}
	id := idParam.AsInt32()

	err := db.Manager.DS.DeleteRecipe(1, id)
	if err != nil {
		utils.LogError("Failed to delete recipe")
	} else {
		utils.JSON(w, http.StatusOK, "")
	}
}
