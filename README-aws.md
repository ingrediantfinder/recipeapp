# AWS DevOps

## Install AWS CLI

Please follow the instruction on AWS site.  The current version used is version 2.  Below is the commands I used.

```bash
# I like to keep all installers in ~/installers
cd ~/installers
curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /

# upgrade awscli if it has been installed before
pip install awscli --upgrade --user
```

Now we need to configure AWS, which consists of two files: `~/.aws/config` and `~/.aws/credentials`.  Please ask the adminstrator for the file `kuosenhao-accessKeys.csv`.

```bash
# put the following in ~/.aws/config
[default]
region = us-west-2
output = json

# put the following in ~/.aws/credentials
[default]
aws_access_key_id = xxxx
aws_secret_access_key = xxxx
```

Below is an excerpt of the AWS instructions on how to download and install `eksctl` with homebrew.

```bash
# install homebrew if it has not been installed
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# install weaveworks
brew tap weaveworks/tap

# install eksctl.  Note, command `xcode-select --install` may need to run to install eksctl successfully
brew install weaveworks/tap/eksctl

# check eksctl version, which should be at least 0.19.0
eksctl version

# if the brew installed version is older than 0.19.0, then one may have to install 
# eksctl manually download an archive of the release from 
# https://github.com/weaveworks/eksctl/releases/download/0.19.0/eksctl_Darwin_amd64.tar.gz
# extract eksctl, and then execute it.
```

If the above installation commands were executed correctly, then the `kubectl` and `aws-iam_authenticator` should already been installed.  If one needs to install `kubectl` manually, please follows instructions in https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html.

## Create a Bujo cluster

Below is an exerpt of https://docs.aws.amazon.com/eks/latest/userguide/fargate-getting-started.html. Be sure to make a copy of ~/.kube/config as `eksctl create cluster` will overwrite it.

```bash
# create a bujo cluster
eksctl create cluster --name bujo-fargate --version 1.16 --region us-west-2 --fargate

$ eksctl create cluster --name bujo-fargate --version 1.16 --region us-west-2 --fargate
[ℹ]  eksctl version 0.19.0
[ℹ]  using region us-west-2
[ℹ]  setting availability zones to [us-west-2b us-west-2a us-west-2d]
[ℹ]  subnets for us-west-2b - public:192.168.0.0/19 private:192.168.96.0/19
[ℹ]  subnets for us-west-2a - public:192.168.32.0/19 private:192.168.128.0/19
[ℹ]  subnets for us-west-2d - public:192.168.64.0/19 private:192.168.160.0/19
[ℹ]  using Kubernetes version 1.16
[ℹ]  creating EKS cluster "bujo-fargate" in "us-west-2" region with Fargate profile
[ℹ]  if you encounter any issues, check CloudFormation console or try 'eksctl utils describe-stacks --region=us-west-2 --cluster=bujo-fargate'
[ℹ]  CloudWatch logging will not be enabled for cluster "bujo-fargate" in "us-west-2"
[ℹ]  you can enable it with 'eksctl utils update-cluster-logging --region=us-west-2 --cluster=bujo-fargate'
[ℹ]  Kubernetes API endpoint access will use default of {publicAccess=true, privateAccess=false} for cluster "bujo-fargate" in "us-west-2"
[ℹ]  1 task: { create cluster control plane "bujo-fargate" }
[ℹ]  building cluster stack "eksctl-bujo-fargate-cluster"
[ℹ]  deploying stack "eksctl-bujo-fargate-cluster"
[ℹ]  waiting for the control plane availability...
[✔]  saved kubeconfig as "/Users/terrancekuo/.kube/config"
[ℹ]  no tasks
[✔]  all EKS cluster resources for "bujo-fargate" have been created
[ℹ]  creating Fargate profile "fp-default" on EKS cluster "bujo-fargate"
[ℹ]  created Fargate profile "fp-default" on EKS cluster "bujo-fargate"
Error: the server could not find the requested resource

# check bujo cluster
eksctl utils describe-stacks --region=us-west-2 --cluster=bujo-fargate

# enable cloud watch.  Did not choose to enable it
# eksctl utils update-cluster-logging --region=us-west-2 --cluster=bujo-fargate

# find the bujo cluster security group
aws eks describe-cluster --name bujo-fargate --query cluster.resourcesVpcConfig.clusterSecurityGroupId

# If coredns pods are stuck in the pending state, do the following
# first create a fargate profile for coredns
aws eks create-fargate-profile --cli-input-json file://coredns.json

# Then patch the coredns pods
kubectl patch deployment coredns -n kube-system --type json -p='[{"op": "remove", "path": "/spec/template/metadata/annotations/eks.amazonaws.com~1compute-type"}]'

```

## ALB Ingress Controller 

https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html
https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/ingress/spec/
https://kubernetes-sigs.github.io/aws-alb-ingress-controller/guide/ingress/annotation/


### Deploy ALB Ingress Controller
```bash
# Create an IAM OIDC (Open ID Connect) provider and associate it with your cluster.
[ℹ]  eksctl version 0.19.0
[ℹ]  using region us-west-2
[ℹ]  will create IAM Open ID Connect provider for cluster "bujo-fargate" in "us-west-2"
[✔]  created IAM Open ID Connect provider for cluster "bujo-fargate" in "us-west-2"

# Create an IAM policy called ALBIngressControllerIAMPolicy for the ALB Ingress Controller pod that 
# allows it to make calls to AWS APIs on your behalf.
# download https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.2/docs/examples/iam-policy.json to $BUJO/aws
cd aws; aws iam create-policy --policy-name ALBIngressControllerIAMPolicy --policy-document file://iam-policy.json

{
    "Policy": {
        "PolicyName": "ALBIngressControllerIAMPolicy",
        "PolicyId": "ANPATPXHAYRGEMPRILBL5",
        "Arn": "arn:aws:iam::239928919116:policy/ALBIngressControllerIAMPolicy",
        "Path": "/",
        "DefaultVersionId": "v1",
        "AttachmentCount": 0,
        "PermissionsBoundaryUsageCount": 0,
        "IsAttachable": true,
        "CreateDate": "2020-05-14T02:59:58+00:00",
        "UpdateDate": "2020-05-14T02:59:58+00:00"
    }
}

# Create a Kubernetes service account named alb-ingress-controller in the kube-system namespace, a cluster role, 
# and a cluster role binding for the ALB Ingress Controller to use
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/rbac-role.yaml

clusterrole.rbac.authorization.k8s.io/alb-ingress-controller created
clusterrolebinding.rbac.authorization.k8s.io/alb-ingress-controller created
serviceaccount/alb-ingress-controller created

# Create an IAM role for the ALB Ingress Controller and attach the role to the service account created in the previous step.
eksctl create iamserviceaccount \
    --region us-west-2 \
    --name alb-ingress-controller \
    --namespace kube-system \
    --cluster bujo-fargate \
    --attach-policy-arn arn:aws:iam::239928919116:policy/ALBIngressControllerIAMPolicy \
    --override-existing-serviceaccounts \
    --approve

[ℹ]  eksctl version 0.19.0
[ℹ]  using region us-west-2
[ℹ]  1 iamserviceaccount (kube-system/alb-ingress-controller) was included (based on the include/exclude rules)
[!]  metadata of serviceaccounts that exist in Kubernetes will be updated, as --override-existing-serviceaccounts was set
[ℹ]  1 task: { 2 sequential sub-tasks: { create IAM role for serviceaccount "kube-system/alb-ingress-controller", create serviceaccount "kube-system/alb-ingress-controller" } }
[ℹ]  building iamserviceaccount stack "eksctl-bujo-fargate-addon-iamserviceaccount-kube-system-alb-ingress-controller"
[ℹ]  deploying stack "eksctl-bujo-fargate-addon-iamserviceaccount-kube-system-alb-ingress-controller"
[ℹ]  serviceaccount "kube-system/alb-ingress-controller" already exists
[ℹ]  updated serviceaccount "kube-system/alb-ingress-controller"

# Deploy the ALB Ingress Controller 

deployment.apps/alb-ingress-controller created

# Open the ALB Ingress Controller deployment manifest for editing with the following command.
kubectl edit deployment.apps/alb-ingress-controller -n kube-system

    spec:
      containers:
      - args:
        - --ingress-class=alb
        - --cluster-name=bujo-fargate
        - --aws-vpc-id=vpc-0bb0d21f12f909701
        - --aws-region=us-west-2

# Confirm that ALB Ingress Controller is running
kubectl get -n kube-system pods 

NAME                                      READY   STATUS    RESTARTS   AGE
alb-ingress-controller-699f47c88f-kkc8w   1/1     Running   1          2m
```

### Deploy a Sample Application to EKS Fargate

```bash
# Create a Fargate profile that includes the sample application's namespace
eksctl create fargateprofile --cluster bujo-fargate --region us-west-2 --name alb-sample-app --namespace 2048-game

# Download and apply the manifest files to create the Kubernetes namespace, deployment, and service
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-deployment.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-service.yaml

# or use the resource .yaml files in ./aws/k8s
kubectl apply -f 2048-game/2048-namespace.yaml
kubectl apply -f 2048-game/2048-deploy.yaml
kubectl apply -f 2048-game/2048-service.yamlm

# Download 2048-ingress.yaml
curl -o 2048-ingress.yaml https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-ingress.yaml
# Edit the 2048-ingress.yaml file. Under the existing alb.ingress.kubernetes.io/scheme: internet-facing line , add the line 
# alb.ingress.kubernetes.io/target-type: ip

# deploy 2048 ingress
kubectl apply -f 2048-game/2048-ingress.yaml

# edit alb-ingress-controller and tail its log
kubectl logs  -f -n kube-system   deployment.apps/alb-ingress-controller

# cleanup the sample 2048 game application
kubectl -n 2048-game delete deployment 2048-deployment
kubectl -n 2048-game delete servie service-2048
kubectl -n 2048-game delete ingress 2048-ingress

kubectl delete -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-ingress.yaml
kubectl delete -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-service.yaml
kubectl delete -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-deployment.yaml
kubectl delete -f https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/2048/2048-namespace.yaml
```

### SSH into an EC2 Node

```bash
ssh -i kuosenhao.pem ec2-user@34.209.120.253
```

## Register Domain and Manage Server Certificate

### Register Domain bojogroup.com

Here is a bit of confusion between the root user 'kuosenhao@gmail.com" and an IAM user 'kuosenhao'.  Since
only the root user 'kuosenhao@gmail.com' has a credit card on file, the 'bujogroup.com' domain is created with 
the root user account in AWS Route 53.

### Manager Server Certification

Login as the IAM user 'kuosenhao and use the AWS Certificate Manager to create a wild certificate for "*.bujo.bujogroup.com", 
which will cover the first level subdomain 'client.bujo.bujogroup.com' and 'prodbackend.bujo.bujogroup.com' but not second or third level
subdomains.  

- Select the 'DNS validate' to allow the Certificate Manager to automatically create CNAME records in 
Route 53.
- Click the 'export DNS configuration to a file' button to export the required DNS configuration to a .csv file.
- Go to the Route 53 page and manully add a CNAME record with the name and the value in the exported .csv file.

### Redeploy the Ingress Resource

Update the bujo-client-ingress-prod.yaml and specify 'alb.*' certificate and SSL related values and host name and run the following
commands

```bash
kubectl delete -n prod ingress bujo-client-prod
kubectl create -f aws/k8s/bujo-client-ingress-prod.yaml
kubectl -n prod get ingress -o wide
```

Note the Ingress' address, login as the root user, and add an 'A' record to route 'client.bujo.bujogroup.com' to the ingress' address.
