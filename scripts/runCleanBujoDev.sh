#!/bin/bash

## Cleans, builds, then runs the recipeapp executable in 'dev' mode port 3000
## Requires Docker.  Automatically pulls the golang image if not available.
##
## Include the argument '--skipAuth' to run issue recipeapp requests without
## having to worry about token/user stuff.
##
## Note: this script executes recipeapp in interactive mode, so it will
## tie up your terminal session and show recipeapp output.  Running this script
## will stop any existing recipeapp docker service before clean building and
## starting a new one in this terminal session.

# Get the directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

##### Docker container managment
CONTAINER_NAME=recipe_app_dev

# Name this terminal tab (works with iTerm as well)
echo -n -e "\033]0;$CONTAINER_NAME\007"

## Kill any existing instance of $CONTAINER_NAME before we run
if [[ `docker ps -q -f name=^$CONTAINER_NAME$` != "" ]]; then
  echo "Stopping existing docker container: $CONTAINER_NAME"
  docker stop $CONTAINER_NAME
  until [[ `docker ps -q -f name=^$CONTAINER_NAME$` == "" ]]
  do
    sleep 1
  done
fi

##### Docker container options
# Port to expose recipeapp to host machine on
PORT=3000
# DB host for recipeapp access
DB_HOST=docker.for.mac.localhost  # Use docker.for.win.localhost on windows machines
# DB port for recipeapp access
DB_PORT=5433
# recipeappds host
DS_HOST=docker.for.mac.localhost
# Where recipeapp folder resides in container
SANDMAN_INSTALL_PATH="/recipeapp"
# Path on local machine that will be mounted to the docker data mnt path
DATA_LOCAL_PATH="$HOME/data/recipeapp"
# Mount point we would like to store/access recipeapp data from within the docker container
DATA_MNT_PATH="$HOME/data/recipeapp"
#echo "DATA_MNT_PATH = $DATA_MNT_PATH"
# Run cmd for golang env to clean build and run recipeapp
RUN_CMD="echo 'cleaning project';go clean;echo 'building recipeapp';go build;echo 'running recipeapp';./recipeapp --enableCORS $@"

# kill this container if this terminal is closed.  This is because the
# interactive docker run cmd won't receive the kill signal if the terminal just
# exits.
trap "docker stop $CONTAINER_NAME > /dev/null 2>&1" 0

# Execute interactive docker and wait

# kill this container if this terminal is closed.  This is because the
# interactive docker run cmd won't receive the kill signal if the terminal just
# exits.
trap "docker stop $CONTAINER_NAME > /dev/null 2>&1" 0

# Execute interactive docker and wait
docker run --rm -it --name $CONTAINER_NAME -v "$DIR/..":$SANDMAN_INSTALL_PATH -v "$DATA_LOCAL_PATH":"$DATA_MNT_PATH" -p $PORT:3000 -w $SANDMAN_INSTALL_PATH -e db.host=$DB_HOST -e db.port=$DB_PORT -e db.maxidle=0 -e DEPLOY_ENV=local golang:1.11.2 /bin/bash -c "$RUN_CMD"

#### Cleanup
# Clear tab name
echo -n -e "\033]0;\007"